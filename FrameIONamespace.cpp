#include "FrameIONamespace.h"
#include <typeinfo>
#include <type_traits>

namespace FrameIONamespace {

    bool CheckVariablesExistenceInStructure(vector<string> VariablesToCheck, vector<string> StructureVariables)
    {
        for (size_t i = 0; i < VariablesToCheck.size(); i++)
        {
            // delete nested values
            std::string delimiter = ".";
            VariablesToCheck[i] = VariablesToCheck[i].substr(0, VariablesToCheck[i].find(delimiter));
            // search if variables exist 
            if (std::find(StructureVariables.begin(), StructureVariables.end(), VariablesToCheck[i]) == StructureVariables.end())
            {
                vector<string>().swap(StructureVariables); // to delete vector
                return false;
            }
        }
        vector<string>().swap(StructureVariables); // to delete vector
        return true;
    }


    /// <summary>
    /// This function takes a vector of string that represent parameters were the code should pharse them from json to c, note these parameters could be nested param like A.B.C were A is not a default C type
    /// </summary>
    /// <param name="response_json"></param>
    /// <param name="Parameters"></param>
    bool AddParsingParametersToJson(json &response_json, vector<string>  Parameters)
    {
        //vector<string> StructureElements;
        //_Account JsonType; 
        //JsonType.IterateElements(StructureElements);


        // adding parameters string into one string 
        string parameters = "\"Parameters\" : [";
        for (size_t i = 0; i < Parameters.size(); i++)
        {
            if(i < Parameters.size() - 1)
                parameters += "\"" + Parameters[i] + "\",";
            else
                parameters += "\"" + Parameters[i] + "\"";
        }
        parameters += "],";

        if (response_json.empty() == true) // in this case the user added some nested parameters that dosen't exist int the parent json, so we can't add the paramerters to non exsiting json, we need to exit
        {
            return false;
        }

        if (response_json.type() == nlohmann::detail::value_t::array)
        {
            int index = 0;
            for (auto it = response_json.begin(); it != response_json.end(); ++it, ++index)
            {
                json temp = *it;
                std::string string_temp = temp.dump();
                string_temp.insert(1, parameters);

                temp = json::parse(string_temp);
                response_json[index] = temp;
            }
        }
        else
        {
            std::string string_temp = response_json.dump();
            string_temp.insert(1, parameters);
            //std::cout << response_json.dump(4) << std::endl;
            response_json = json::parse(string_temp);
        }

        //response_json.insert(response_json[0].begin(), parameters);
        //std::cout << response_json.dump(4) << std::endl;
        return true;
    }

    /// <summary>
    /// This function will return true if the MainString equal to ParentString.XXXXXX
    /// and the MainString will be at the end equal to XXXXXX
    /// </summary>
    bool SplitNestedParameters(string &MainString, string ParentString) {

        if (MainString.find(ParentString, 0) == 0) // check if MainString containe the ParentString
        {
            size_t index = MainString.find(".", 0);
            if (index != std::string::npos)
            {
                MainString = MainString.substr(index + 1);
                return true;
            }
            return false; // child string is empty
        }
        return false; // parent string not found
    }

    /// <summary>
    /// Extract Nested Parmaters form Vector List, ex _account_settings._type , _account_settings.id
    /// MainParameters start at MainParametersStartingIndex were the ParentParameter = _account_settings is pointed in MainParametersStartingIndex
    /// and it should save in NestedParameters _type, id
    /// </summary>
    /// <param name="MainParameters"></param>
    /// <param name="NestedParameters"></param>
    /// <param name="ParentParameter"></param>
    /// <param name="MainParametersStartingIndex"></param>
    /// <returns></returns>
    bool ExtractNestedParameters(vector<string> MainParameters, vector<string>& NestedParameters, string ParentParameter, int& MainParametersStartingIndex)
    {
        // since this not a default type we need to check if the user passed nested parameters for this type as well
        // nested parameters should be inserted in this way : account_settings."Parmater"
        bool TotalResult = false; // it represent if any casting has been made successfully or not at all
        bool splitResult = false; // it represent each casting status
        int counter = 1;
        do
        {
            if (MainParametersStartingIndex < MainParameters.size())
            {
                splitResult = SplitNestedParameters(MainParameters[MainParametersStartingIndex], ParentParameter);
                if (splitResult == true)
                {
                    NestedParameters.push_back(MainParameters[MainParametersStartingIndex]);
                    MainParametersStartingIndex++;
                    TotalResult = true;
                }
            }
            else
            {
                break;
            }
        } while (splitResult);
        return TotalResult;
    }

    /// <summary>
    /// This function is used wen we need to update the nested types in a Json formate with it own parameters that we needs to retive.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="Json"></param>
    /// <param name="CStructure"></param>
    /// <param name="index"></param>
    /// <param name="UpdatedIndex"></param>
    /// <returns></returns>
    template <typename T>
    bool UpdateJsonWithNestedParameters(json &Json, T &CStructure, int index, int &TotalIndex) {
        vector<string> NestedParameters;
        int Nestedindex = index + 1;
        if (ExtractNestedParameters(CStructure.Parameters, NestedParameters, CStructure.Parameters[index], Nestedindex))
        {
            json nestedJson = Json[CStructure.Parameters[index]];
            //std::cout << nestedJson.dump(4) << std::endl;
            if (AddParsingParametersToJson(nestedJson, NestedParameters))
            {
                Json[CStructure.Parameters[index]] = nestedJson;
                //std::cout << j.dump(4) << std::endl;
                //need to update the index since we have successfully added pharsing parameters into a nested varaiable in json
                TotalIndex = Nestedindex - 1;
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    template<typename Base, typename T>
    inline bool instanceof(const T*) {
        return std::is_base_of<Base, T>::value;
    }

    /// <summary>
    /// check if type is a element type
    /// we need to check pointer and null objects as well
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="type"></param>
    /// <returns></returns>
    template <typename T>
    bool IsPrimaryElement(T type) {

        string typeName = typeid(type).name();
        //cout << typeid(type).name() << endl;

        // check if T is a float
        if (instanceof<float>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of float
        else if (instanceof<float*>(&type) == true)
        {
            return true;
        }
        // check if T is a int
        else if (instanceof<int>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of int
        else if (instanceof<int*>(&type) == true)
        {
            return true;
        }
        // check if T is a double
        else if (instanceof<double>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of double
        else if (instanceof<double*>(&type) == true)
        {
            return true;
        }
        // check if T is a char
        else if (instanceof<char>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of char
        else if (instanceof<char*>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of bool
        else if (typeName.compare("bool") == 0) // instanceof dosen't work for boll 
        {
            return true;
        }
        // check if T is a pointer of bool
        else if (instanceof<bool*>(&type) == true)
        {
            return true;
        }
        // check if T is a string
        else if (instanceof<string>(&type) == true)
        {
            return true;
        }
        // check if T is a vector of int
        else if (instanceof<vector<int>>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of vector of int
        else if (instanceof<vector<int>*>(&type) == true)
        {
            return true;
        }
        // check if T is a vector of float
        else if (instanceof<vector<float>>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of vector of float
        else if (instanceof<vector<float>*>(&type) == true)
        {
            return true;
        }
        // check if T is a vector of double
        else if (instanceof<vector<double>>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of vector of double
        else if (instanceof<vector<double>*>(&type) == true)
        {
            return true;
        }
        // check if T is a vector of string
        else if (instanceof<vector<string>>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of vector of string
        else if (instanceof<vector<string>*>(&type) == true)
        {
            return true;
        }
        // check if T is a vector of char
        else if (instanceof<vector<char>>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of vector of char
        else if (instanceof<vector<char>*>(&type) == true)
        {
            return true;
        }
        // check if T is a vector of bool
        else if (instanceof<vector<bool>>(&type) == true)
        {
            return true;
        }
        // check if T is a pointer of vector of chboolar
        else if (instanceof<vector<bool>*>(&type) == true)
        {
            return true;
        }
        //// check if T is a null pointer
        //else if (instanceof<nullptr>(&type) == true)
        //{
        //    return true;
        //}
        // check if T is a null nullptr_t
        else if (instanceof<nullptr_t>(&type) == true)
        {
            return true;
        }
        return false;
    }

    template <typename T, typename L> 
    bool ParseJsonToC(T &Ctype, L &CStruct, json &json_, int &index) {
        int TotalIndex = index;
        if(IsPrimaryElement<T>(Ctype))
        { 
            if (json_.at(CStruct.Parameters[index]).type() != nlohmann::detail::value_t::null) {
                json_.at(CStruct.Parameters[index]).get_to(Ctype);
                return true;
            }
            return false;
        }

        if (UpdateJsonWithNestedParameters<L>(json_, CStruct, index, TotalIndex) == true)
        {
            if (json_.at(CStruct.Parameters[index]).type() != nlohmann::detail::value_t::null) {
                json_.at(CStruct.Parameters[index]).get_to(Ctype);
            }
            index = TotalIndex; // point i to the latest parameter that didn't been parsed yet
            return true;
        }
        return false;
    }


    void to_json(json& j, const _Authorization& p)
    {
        j = json{ {"_type", p._type},
                  {"account_id", p.account_id},
                  {"bio", p.bio},
                  {"context", p.context},
                  {"deleted_at", p.deleted_at},
                  {"digest_frequency", p.digest_frequency},
                  {"email", p.email},
                  {"email_confirm_by", p.email_confirm_by},
                  {"email_preferences", p.email_preferences},
                  {"features_seen", p.features_seen},
                  {"first_login_at", p.first_login_at},
                  {"from_google", p.from_google},
                  {"highest_account_role", p.highest_account_role},
                  {"id", p.id},
                  {"image_128", p.image_128},
                  {"image_256", p.image_256},
                  {"image_32", p.image_32},
                  {"image_64", p.image_64},
                  {"inserted_at", p.inserted_at},
                  {"joined_via", p.joined_via},
                  {"last_seen", p.last_seen},
                  {"location", p.location},
                  {"mfa_enforced_at", p.mfa_enforced_at},
                  {"name", p.name},
                  {"next_digest_date", p.next_digest_date},
                  {"phone", p.phone},
                  {"profile_image", p.profile_image},
                  {"profile_image_original", p.profile_image_original},
                  {"roles", p.roles},
                  {"timezone_value", p.timezone_value},
                  {"updated_at", p.updated_at},
                  {"upload_url", p.upload_url},
                  {"user_default_color", p.user_default_color},
                  {"user_hash", p.user_hash} };
    }

    void from_json(const json& jj, _Authorization& p) {

        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type") {
                if (ParseJsonToC<decltype(p._type), _Authorization>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account_id") {
                if (ParseJsonToC<decltype(p.account_id), _Authorization>(p.account_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "bio") {
                if (ParseJsonToC<decltype(p.bio), _Authorization>(p.bio, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "context") {
                if (ParseJsonToC<decltype(p.context), _Authorization>(p.context, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "deleted_at") {
                if (ParseJsonToC<decltype(p.deleted_at), _Authorization>(p.deleted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "digest_frequency") {
                if (ParseJsonToC<decltype(p.digest_frequency), _Authorization>(p.digest_frequency, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "email") {
                if (ParseJsonToC<decltype(p.email), _Authorization>(p.email, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "email_confirm_by") {
                if (ParseJsonToC<decltype(p.email_confirm_by), _Authorization>(p.email_confirm_by, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "email_preferences") {
                if (ParseJsonToC<decltype(p.email_preferences), _Authorization>(p.email_preferences, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "features_seen") {
                if (ParseJsonToC<decltype(p.features_seen), _Authorization>(p.features_seen, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "first_login_at") {
                if (ParseJsonToC<decltype(p.first_login_at), _Authorization>(p.first_login_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "from_google") {
                if (ParseJsonToC<decltype(p.from_google), _Authorization>(p.from_google, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "highest_account_role") {
                if (ParseJsonToC<decltype(p.highest_account_role), _Authorization>(p.highest_account_role, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id") {
                if (ParseJsonToC<decltype(p.id), _Authorization>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_128") {
                if (ParseJsonToC<decltype(p.image_128), _Authorization>(p.image_128, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_256") {
                if (ParseJsonToC<decltype(p.image_256), _Authorization>(p.image_256, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_32") {
                if (ParseJsonToC<decltype(p.image_32), _Authorization>(p.image_32, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_64") {
                if (ParseJsonToC<decltype(p.image_64), _Authorization>(p.image_64, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "inserted_at") {
                if (ParseJsonToC<decltype(p.inserted_at), _Authorization>(p.inserted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "joined_via") {
                if (ParseJsonToC<decltype(p.joined_via), _Authorization>(p.joined_via, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "last_seen") {
                if (ParseJsonToC<decltype(p.last_seen), _Authorization>(p.last_seen, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "location") {
                if (ParseJsonToC<decltype(p.location), _Authorization>(p.location, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "mfa_enforced_at") {
                if (ParseJsonToC<decltype(p.mfa_enforced_at), _Authorization>(p.mfa_enforced_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name") {
                if (ParseJsonToC<decltype(p.name), _Authorization>(p.name, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "next_digest_date") {
                if (ParseJsonToC<decltype(p.next_digest_date), _Authorization>(p.next_digest_date, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "phone") {
                if (ParseJsonToC<decltype(p.phone), _Authorization>(p.phone, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "profile_image") {
                if (ParseJsonToC<decltype(p.profile_image), _Authorization>(p.profile_image, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "profile_image_original") {
                if (ParseJsonToC<decltype(p.profile_image_original), _Authorization>(p.profile_image_original, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "roles") {
                if (ParseJsonToC<decltype(p.roles), _Authorization>(p.roles, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "timezone_value") {
                if (ParseJsonToC<decltype(p.timezone_value), _Authorization>(p.timezone_value, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "updated_at") {
                if (ParseJsonToC<decltype(p.updated_at), _Authorization>(p.updated_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "upload_url") {
                if (ParseJsonToC<decltype(p.upload_url), _Authorization>(p.upload_url, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_default_color") {
                if (ParseJsonToC<decltype(p.user_default_color), _Authorization>(p.user_default_color, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_hash") {
                if (ParseJsonToC<decltype(p.user_hash), _Authorization>(p.user_hash, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _account_settings& p)
    {
        j = json{ {"_type", p._type},
                  {"default_sharing_to_public", p.default_sharing_to_public},
                  {"download_min_role", p.download_min_role},
                  {"id", p.id},
                  {"internal_sbwm_template_id", p.internal_sbwm_template_id},
                  {"public_sharing_min_role", p.public_sharing_min_role},
                  {"sbwm_end_user_override", p.sbwm_end_user_override},
                  {"sbwm_require_internally", p.sbwm_require_internally},
                  {"sbwm_require_sharing", p.sbwm_require_sharing},
                  {"share_sbwm_template_id", p.share_sbwm_template_id},
                  {"unwatermarked_internal_access_min_role", p.unwatermarked_internal_access_min_role},
                  {"unwatermarked_share_min_role", p.unwatermarked_share_min_role} };
    }

    void from_json(const json& jj, _account_settings& p) {
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type"){
                if (ParseJsonToC<decltype(p._type), _account_settings>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "default_sharing_to_public") {
                if (ParseJsonToC<decltype(p.default_sharing_to_public), _account_settings>(p.default_sharing_to_public, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "download_min_role") {
                if (ParseJsonToC<decltype(p.download_min_role), _account_settings>(p.download_min_role, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id") {
                if (ParseJsonToC<decltype(p.id), _account_settings>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "internal_sbwm_template_id") {
                if (ParseJsonToC<decltype(p.internal_sbwm_template_id), _account_settings>(p.internal_sbwm_template_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "public_sharing_min_role") {
                if (ParseJsonToC<decltype(p.public_sharing_min_role), _account_settings>(p.public_sharing_min_role, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "sbwm_end_user_override") {
                if (ParseJsonToC<decltype(p.sbwm_end_user_override), _account_settings>(p.sbwm_end_user_override, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "sbwm_require_internally") {
                if (ParseJsonToC<decltype(p.sbwm_require_internally), _account_settings>(p.sbwm_require_internally, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "sbwm_require_sharing") {
                if (ParseJsonToC<decltype(p.sbwm_require_sharing), _account_settings>(p.sbwm_require_sharing, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "share_sbwm_template_id") {
                if (ParseJsonToC<decltype(p.share_sbwm_template_id), _account_settings>(p.share_sbwm_template_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "unwatermarked_internal_access_min_role") {
                if (ParseJsonToC<decltype(p.unwatermarked_internal_access_min_role), _account_settings>(p.unwatermarked_internal_access_min_role, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "unwatermarked_share_min_role") {
                if (ParseJsonToC<decltype(p.unwatermarked_share_min_role), _account_settings>(p.unwatermarked_share_min_role, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _available_features& p) {
        j = json{ {"4k_playback", p._4k_playback},
              {"archival_storage", p.archival_storage},
              {"custom_branded_emails", p.custom_branded_emails},
              {"custom_branded_presentations", p.custom_branded_presentations},
              {"devices", p.devices},
              {"hdr", p.hdr},
              {"id", p.id},
              {"password_protected_shares", p.password_protected_shares},
              {"private_projects", p.private_projects},
              {"reel_player", p.reel_player},
              {"secure_sharing", p.secure_sharing},
              {"session_based_watermarking", p.session_based_watermarking},
              {"share_link_expiration", p.share_link_expiration},
              {"team_only_comments", p.team_only_comments} };
    }

    void from_json(const json& jj, _available_features& p) {
        json json_ = jj; 
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type") {
                if (ParseJsonToC<decltype(p._type), _available_features>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "4k_playback") {
                if (ParseJsonToC<decltype(p._4k_playback), _available_features>(p._4k_playback, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "archival_storage") {
                if (ParseJsonToC<decltype(p.archival_storage), _available_features>(p.archival_storage, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "custom_branded_emails") {
                if (ParseJsonToC<decltype(p.custom_branded_emails), _available_features>(p.custom_branded_emails, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "custom_branded_presentations") {
                if (ParseJsonToC<decltype(p.custom_branded_presentations), _available_features>(p.custom_branded_presentations, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "devices") {
                if (ParseJsonToC<decltype(p.devices), _available_features>(p.devices, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "hdr") {
                if (ParseJsonToC<decltype(p.hdr), _available_features>(p.hdr, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id") {
                if (ParseJsonToC<decltype(p.id), _available_features>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "password_protected_shares") {
                if (ParseJsonToC<decltype(p.password_protected_shares), _available_features>(p.password_protected_shares, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "private_projects") {
                if (ParseJsonToC<decltype(p.private_projects), _available_features>(p.private_projects, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "reel_player") {
                if (ParseJsonToC<decltype(p.reel_player), _available_features>(p.reel_player, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "secure_sharing") {
                if (ParseJsonToC<decltype(p.secure_sharing), _available_features>(p.secure_sharing, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "session_based_watermarking") {
                if (ParseJsonToC<decltype(p.session_based_watermarking), _available_features>(p.session_based_watermarking, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "share_link_expiration") {
                if (ParseJsonToC<decltype(p.share_link_expiration), _available_features>(p.share_link_expiration, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "team_only_comments") {
                if (ParseJsonToC<decltype(p.team_only_comments), _available_features>(p.team_only_comments, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _offline_config& p) {
        j = json{ {"asset_expiry", p.asset_expiry},
              {"id", p.id},
              {"logged_in_limit", p.logged_in_limit} };
    }

    void from_json(const json& jj, _offline_config& p) {
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "asset_expiry") {
                if (ParseJsonToC<decltype(p.asset_expiry), _offline_config>(p.asset_expiry, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id") {
                if (ParseJsonToC<decltype(p.id), _offline_config>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "logged_in_limit") {
                if (ParseJsonToC<decltype(p.logged_in_limit), _offline_config>(p.logged_in_limit, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _owner& p)
    {
        j = json{ {"_type", p._type},
                  {"account_id", p.account_id},
                  {"bio", p.bio},
                  {"context", p.context},
                  {"deleted_at", p.deleted_at},
                  {"digest_frequency", p.digest_frequency},
                  {"email", p.email},
                  {"email_confirm_by", p.email_confirm_by},
                  {"email_preferences", p.email_preferences},
                  {"features_seen", p.features_seen},
                  {"first_login_at", p.first_login_at},
                  {"from_google", p.from_google},
                  {"highest_account_role", p.highest_account_role},
                  {"id", p.id},
                  {"image_128", p.image_128},
                  {"image_256", p.image_256},
                  {"image_32", p.image_32},
                  {"image_64", p.image_64},
                  {"inserted_at", p.inserted_at},
                  {"joined_via", p.joined_via},
                  {"last_seen", p.last_seen},
                  {"location", p.location},
                  {"mfa_enforced_at", p.mfa_enforced_at},
                  {"name", p.name},
                  {"next_digest_date", p.next_digest_date},
                  {"phone", p.phone},
                  {"profile_image", p.profile_image},
                  {"profile_image_original", p.profile_image_original},
                  {"roles", p.roles},
                  {"timezone_value", p.timezone_value},
                  {"updated_at", p.updated_at},
                  {"upload_url", p.upload_url},
                  {"user_default_color", p.user_default_color} };
    }

    void from_json(const json& jj, _owner& p) {
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type") {
                if (ParseJsonToC<decltype(p._type), _owner>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account_id") {
                if (ParseJsonToC<decltype(p.account_id), _owner>(p.account_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "bio") {
                if (ParseJsonToC<decltype(p.bio), _owner>(p.bio, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "context") {
                if (ParseJsonToC<decltype(p.context), _owner>(p.context, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "deleted_at") {
                if (ParseJsonToC<decltype(p.deleted_at), _owner>(p.deleted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "digest_frequency") {
                if (ParseJsonToC<decltype(p.digest_frequency), _owner>(p.digest_frequency, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "email") {
                if (ParseJsonToC<decltype(p.email), _owner>(p.email, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "email_confirm_by") {
                if (ParseJsonToC<decltype(p.email_confirm_by), _owner>(p.email_confirm_by, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "email_preferences") {
                if (ParseJsonToC<decltype(p.email_preferences), _owner>(p.email_preferences, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "features_seen") {
                if (ParseJsonToC<decltype(p.features_seen), _owner>(p.features_seen, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "first_login_at") {
                if (ParseJsonToC<decltype(p.first_login_at), _owner>(p.first_login_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "from_google") {
                if (ParseJsonToC<decltype(p.from_google), _owner>(p.from_google, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "highest_account_role") {
                if (ParseJsonToC<decltype(p.highest_account_role), _owner>(p.highest_account_role, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id") {
                if (ParseJsonToC<decltype(p.id), _owner>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_128") {
                if (ParseJsonToC<decltype(p.image_128), _owner>(p.image_128, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_256") {
                if (ParseJsonToC<decltype(p.image_256), _owner>(p.image_256, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_32") {
                if (ParseJsonToC<decltype(p.image_32), _owner>(p.image_32, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_64") {
                if (ParseJsonToC<decltype(p.image_64), _owner>(p.image_64, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "inserted_at") {
                if (ParseJsonToC<decltype(p.inserted_at), _owner>(p.inserted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "joined_via") {
                if (ParseJsonToC<decltype(p.joined_via), _owner>(p.joined_via, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "last_seen") {
                if (ParseJsonToC<decltype(p.last_seen), _owner>(p.last_seen, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "location") {
                if (ParseJsonToC<decltype(p.location), _owner>(p.location, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "mfa_enforced_at") {
                if (ParseJsonToC<decltype(p.mfa_enforced_at), _owner>(p.mfa_enforced_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name") {
                if (ParseJsonToC<decltype(p.name), _owner>(p.name, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "next_digest_date") {
                if (ParseJsonToC<decltype(p.next_digest_date), _owner>(p.next_digest_date, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "phone") {
                if (ParseJsonToC<decltype(p.phone), _owner>(p.phone, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "profile_image") {
                if (ParseJsonToC<decltype(p.profile_image), _owner>(p.profile_image, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "profile_image_original") {
                if (ParseJsonToC<decltype(p.profile_image_original), _owner>(p.profile_image_original, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "roles") {
                if (ParseJsonToC<decltype(p.roles), _owner>(p.roles, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "timezone_value") {
                if (ParseJsonToC<decltype(p.timezone_value), _owner>(p.timezone_value, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "updated_at") {
                if (ParseJsonToC<decltype(p.updated_at), _owner>(p.updated_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "upload_url") {
                if (ParseJsonToC<decltype(p.upload_url), _owner>(p.upload_url, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_default_color") {
                if (ParseJsonToC<decltype(p.user_default_color), _owner>(p.user_default_color, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _plan& p)
    {
        j = json{ {"_type", p._type},
                  {"archived_storage_limit", p.archived_storage_limit},
                  {"autoscaling", p.autoscaling},
                  {"available_features", p.available_features},
                  {"collaborator_limit", p.collaborator_limit},
                  {"cost", p.cost},
                  {"default_plan", p.default_plan},
                  {"deleted_at", p.deleted_at},
                  {"enterprise", p.enterprise},
                  {"file_limit", p.file_limit},
                  {"id", p.id},
                  {"inserted_at", p.inserted_at},
                  {"lifetime_file_limit", p.lifetime_file_limit},
                  {"member_limit", p.member_limit},
                  {"name", p.name},
                  {"payment_method", p.payment_method},
                  {"period", p.period},
                  {"project_limit", p.project_limit},
                  {"storage_limit", p.storage_limit},
                  {"team_limit", p.team_limit},
                  {"tier", p.tier},
                  {"title", p.title},
                  {"updated_at", p.updated_at},
                  {"user_limit", p.user_limit},
                  {"user_max", p.user_max},
                  {"version", p.version} };
    }

    void from_json(const json& jj, _plan& p) {
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type") {
                if (ParseJsonToC<decltype(p._type), _plan>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "archived_storage_limit") {
                if (ParseJsonToC<decltype(p.archived_storage_limit), _plan>(p.archived_storage_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "autoscaling") {
                if (ParseJsonToC<decltype(p.autoscaling), _plan>(p.autoscaling, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "available_features") {
                if (ParseJsonToC<decltype(p.available_features), _plan>(p.available_features, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "collaborator_limit") {
                if (ParseJsonToC<decltype(p.collaborator_limit), _plan>(p.collaborator_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "cost") {
                if (ParseJsonToC<decltype(p.cost), _plan>(p.cost, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "default_plan") {
                if (ParseJsonToC<decltype(p.default_plan), _plan>(p.default_plan, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "deleted_at") {
                if (ParseJsonToC<decltype(p.deleted_at), _plan>(p.deleted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "enterprise") {
                if (ParseJsonToC<decltype(p.enterprise), _plan>(p.enterprise, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "file_limit") {
                if (ParseJsonToC<decltype(p.file_limit), _plan>(p.file_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id") {
                if (ParseJsonToC<decltype(p.id), _plan>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "inserted_at") {
                if (ParseJsonToC<decltype(p.inserted_at), _plan>(p.inserted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "lifetime_file_limit") {
                if (ParseJsonToC<decltype(p.lifetime_file_limit), _plan>(p.lifetime_file_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "member_limit") {
                if (ParseJsonToC<decltype(p.member_limit), _plan>(p.member_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name") {
                if (ParseJsonToC<decltype(p.name), _plan>(p.name, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "payment_method") {
                if (ParseJsonToC<decltype(p.payment_method), _plan>(p.payment_method, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "period") {
                if (ParseJsonToC<decltype(p.period), _plan>(p.period, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "project_limit") {
                if (ParseJsonToC<decltype(p.project_limit), _plan>(p.project_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "storage_limit") {
                if (ParseJsonToC<decltype(p.storage_limit), _plan>(p.storage_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "team_limit") {
                if (ParseJsonToC<decltype(p.team_limit), _plan>(p.team_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "tier") {
                if (ParseJsonToC<decltype(p.tier), _plan>(p.tier, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "title") {
                if (ParseJsonToC<decltype(p.title), _plan>(p.title, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "updated_at") {
                if (ParseJsonToC<decltype(p.updated_at), _plan>(p.updated_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_limit") {
                if (ParseJsonToC<decltype(p.user_limit), _plan>(p.user_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_max") {
                if (ParseJsonToC<decltype(p.user_max), _plan>(p.user_max, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "version") {
                if (ParseJsonToC<decltype(p.version), _plan>(p.version, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _promotion& p)
    {
        j = json{ {"_type", p._type},
                  {"autoscaling", p.autoscaling},
                  {"can_override_limitations", p.can_override_limitations},
                  {"deleted_at", p.deleted_at},
                  {"expires_at", p.expires_at},
                  {"header_subtext", p.header_subtext},
                  {"header_text", p.header_text},
                  {"id", p.id},
                  {"inserted_at", p.inserted_at},
                  {"is_trial", p.is_trial},
                  {"new_price", p.new_price},
                  {"no_credit_card", p.no_credit_card},
                  {"plan_id", p.plan_id},
                  {"promo_code", p.promo_code},
                  {"submit_text", p.submit_text},
                  {"trial_length", p.trial_length},
                  {"updated_at", p.updated_at} };
    }

    void from_json(const json& jj, _promotion& p) {
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _promotion>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "autoscaling")
            {
                if (ParseJsonToC<decltype(p.autoscaling), _promotion>(p.autoscaling, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "can_override_limitations")
            {
                if (ParseJsonToC<decltype(p.can_override_limitations), _promotion>(p.can_override_limitations, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "deleted_at")
            {
                if (ParseJsonToC<decltype(p.deleted_at), _promotion>(p.deleted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "expires_at")
            {
                if (ParseJsonToC<decltype(p.expires_at), _promotion>(p.expires_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "header_subtext")
            {
                if (ParseJsonToC<decltype(p.header_subtext), _promotion>(p.header_subtext, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "header_text")
            {
                if (ParseJsonToC<decltype(p.header_text), _promotion>(p.header_text, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _promotion>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "inserted_at")
            {
                if (ParseJsonToC<decltype(p.inserted_at), _promotion>(p.inserted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "is_trial")
            {
                if (ParseJsonToC<decltype(p.is_trial), _promotion>(p.is_trial, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "new_price")
            {
                if (ParseJsonToC<decltype(p.new_price), _promotion>(p.new_price, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "no_credit_card")
            {
                if (ParseJsonToC<decltype(p.no_credit_card), _promotion>(p.no_credit_card, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "plan_id")
            {
                if (ParseJsonToC<decltype(p.plan_id), _promotion>(p.plan_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "promo_code")
            {
                if (ParseJsonToC<decltype(p.promo_code), _promotion>(p.promo_code, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "submit_text")
            {
                if (ParseJsonToC<decltype(p.submit_text), _promotion>(p.submit_text, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "trial_length")
            {
                if (ParseJsonToC<decltype(p.trial_length), _promotion>(p.trial_length, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "updated_at")
            {
                if (ParseJsonToC<decltype(p.updated_at), _promotion>(p.updated_at, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _subscription& p)
    {
        j = json{ {"_type", p._type},
                  {"account_id", p.account_id},
                  {"archived_storage_limit", p.archived_storage_limit},
                  {"balance", p.balance},
                  {"cancellation_option", p.cancellation_option},
                  {"cancellation_reason", p.cancellation_reason},
                  {"cancelled_at", p.cancelled_at},
                  {"deleted_at", p.deleted_at},
                  {"id", p.id},
                  {"inserted_at", p.inserted_at},
                  {"last_payment_at", p.last_payment_at},
                  {"member_limit", p.member_limit},
                  {"next_bill_at", p.next_bill_at},
                  {"on_trial", p.on_trial},
                  {"plan", p.plan},
                  {"plan_id", p.plan_id},
                //{"promotion", p.promotion},
                  {"promotion_expires_at", p.promotion_expires_at},
                  {"promotion_id", p.promotion_id},
                  {"requires_autoscaling", p.requires_autoscaling},
                  {"storage_limit", p.storage_limit},
                  {"subscription_end_at", p.subscription_end_at},
                  {"total_archived_storage_limit", p.total_archived_storage_limit},
                  {"total_lifetime_file_limit", p.total_lifetime_file_limit},
                  {"total_member_limit", p.total_member_limit},
                  {"total_project_limit", p.total_project_limit},
                  {"total_storage_limit", p.total_storage_limit},
                  { "total_user_limit", p.total_user_limit },
                  { "updated_at", p.updated_at },
                  { "user_limit", p.user_limit } };
    }

    void from_json(const json& jj, _subscription& p) {
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _subscription>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account_id") {
                if (ParseJsonToC<decltype(p.account_id), _subscription>(p.account_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "archived_storage_limit") {
                if (ParseJsonToC<decltype(p.archived_storage_limit), _subscription>(p.archived_storage_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "balance") {
                if (ParseJsonToC<decltype(p.balance), _subscription>(p.balance, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "cancellation_option") {
                if (ParseJsonToC<decltype(p.cancellation_option), _subscription>(p.cancellation_option, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "cancellation_reason") {
                if (ParseJsonToC<decltype(p.cancellation_reason), _subscription>(p.cancellation_reason, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "cancelled_at") {
                if (ParseJsonToC<decltype(p.cancelled_at), _subscription>(p.cancelled_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "deleted_at") {
                if (ParseJsonToC<decltype(p.deleted_at), _subscription>(p.deleted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id") {
                if (ParseJsonToC<decltype(p.id), _subscription>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "inserted_at") {
                if (ParseJsonToC<decltype(p.inserted_at), _subscription>(p.inserted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "last_payment_at") {
                if (ParseJsonToC<decltype(p.last_payment_at), _subscription>(p.last_payment_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "member_limit") {
                if (ParseJsonToC<decltype(p.member_limit), _subscription>(p.member_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "next_bill_at") {
                if (ParseJsonToC<decltype(p.next_bill_at), _subscription>(p.next_bill_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "on_trial") {
                if (ParseJsonToC<decltype(p.on_trial), _subscription>(p.on_trial, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "plan") {
                if (ParseJsonToC<decltype(p.plan), _subscription>(p.plan, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "plan_id") {
                if (ParseJsonToC<decltype(p.plan_id), _subscription>(p.plan_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "promotion") {
                if (ParseJsonToC<decltype(p.promotion), _subscription>(p.promotion, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "promotion_expires_at") {
                if (ParseJsonToC<decltype(p.promotion_expires_at), _subscription>(p.promotion_expires_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "promotion_id") {
                if (ParseJsonToC<decltype(p.promotion_id), _subscription>(p.promotion_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "requires_autoscaling") {
                if (ParseJsonToC<decltype(p.requires_autoscaling), _subscription>(p.requires_autoscaling, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "storage_limit") {
                if (ParseJsonToC<decltype(p.storage_limit), _subscription>(p.storage_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "subscription_end_at") {
                if (ParseJsonToC<decltype(p.subscription_end_at), _subscription>(p.subscription_end_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "total_archived_storage_limit") {
                if (ParseJsonToC<decltype(p.total_archived_storage_limit), _subscription>(p.total_archived_storage_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "total_lifetime_file_limit") {
                if (ParseJsonToC<decltype(p.total_lifetime_file_limit), _subscription>(p.total_lifetime_file_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "total_member_limit") {
                if (ParseJsonToC<decltype(p.total_member_limit), _subscription>(p.total_member_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "total_project_limit") {
                if (ParseJsonToC<decltype(p.total_project_limit), _subscription>(p.total_project_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "total_storage_limit") {
                if (ParseJsonToC<decltype(p.total_storage_limit), _subscription>(p.total_storage_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "total_user_limit") {
                if (ParseJsonToC<decltype(p.total_user_limit), _subscription>(p.total_user_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "updated_at") {
                if (ParseJsonToC<decltype(p.updated_at), _subscription>(p.updated_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_limit") {
                if (ParseJsonToC<decltype(p.user_limit), _subscription>(p.user_limit, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _Account& p)
    {
        j = json{ {"_type", p._type},
                  {"account_default_color", p.account_default_color},
                  {"account_settings", p.account_settings},
                  {"archived_storage", p.archived_storage},
                  {"available_features", p.available_features},
                  {"billing_emails", p.billing_emails},
                  {"city", p.city},
                  {"collaborator_count", p.collaborator_count},
                  {"collaborator_role_count", p.collaborator_role_count},
                  {"company_address", p.company_address},
                  {"company_name", p.company_name},
                  {"country", p.country},
                  {"deleted_at", p.deleted_at},
                  {"delinquent_at", p.delinquent_at},
                  {"display_name", p.display_name},
                  {"duration", p.duration},
                  {"file_count", p.file_count},
                  {"folder_count", p.folder_count},
                  {"frames", p.frames},
                  {"id", p.id},
                  {"image", p.image},
                  {"image_128", p.image_128},
                  {"image_256", p.image_256},
                  {"image_32", p.image_32},
                  {"image_64", p.image_64},
                  {"inserted_at", p.inserted_at},
                  {"invoice_emails", p.invoice_emails},
                  { "lifetime_file_count", p.lifetime_file_count },
                  { "line1", p.line1 },
                  { "line2", p.line2 },
                  {"locked_at", p.locked_at},
                  { "member_count", p.member_count },
                  { "mfa_enforced_at", p.mfa_enforced_at },
                  { "offline_config", p.offline_config },
                  { "owner", p.owner },
                  { "owner_id", p.owner_id },
                  { "plan", p.plan },
                  { "postal_code", p.postal_code },
                  { "project_count", p.project_count },
                  { "resource_id", p.resource_id },
                  { "revenue_reported_at", p.revenue_reported_at },
                  { "reviewer_count", p.reviewer_count },
                  { "state", p.state },
                  { "storage", p.storage },
                  { "subscription", p.subscription },
                  { "team_count", p.team_count },
                  { "unpaid_at", p.unpaid_at },
                  { "updated_at", p.updated_at },
                  { "upload_url", p.upload_url },
                  { "user_count", p.user_count },
                  { "vat", p.vat },
                  { "version", p.version } };
    }

    void from_json(const json& jj, _Account& p) {

        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _Account>(p._type, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "account_default_color")
            {
                if (ParseJsonToC<decltype(p.account_default_color), _Account>(p.account_default_color, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "account_settings")
            {
                if (ParseJsonToC<decltype(p.account_settings), _Account>(p.account_settings, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "archived_storage")
            {
                if (ParseJsonToC<decltype(p.archived_storage), _Account>(p.archived_storage, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "available_features")
            {
                if (ParseJsonToC<decltype(p.available_features), _Account>(p.available_features, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "billing_emails")
            {
                if (ParseJsonToC<decltype(p.billing_emails), _Account>(p.billing_emails, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "city")
            {
                if (ParseJsonToC<decltype(p.city), _Account>(p.city, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "collaborator_count")
            {
                if (ParseJsonToC<decltype(p.collaborator_count), _Account>(p.collaborator_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "collaborator_role_count")
            {
                if (ParseJsonToC<decltype(p.collaborator_role_count), _Account>(p.collaborator_role_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "company_address")
            {
                if (ParseJsonToC<decltype(p.company_address), _Account>(p.company_address, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "company_name")
            {
                if (ParseJsonToC<decltype(p.company_name), _Account>(p.company_name, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "country")
            {
                if (ParseJsonToC<decltype(p.country), _Account>(p.country, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "deleted_at")
            {
                if (ParseJsonToC<decltype(p.deleted_at), _Account>(p.deleted_at, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "delinquent_at")
            {
                if (ParseJsonToC<decltype(p.delinquent_at), _Account>(p.delinquent_at, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "display_name")
            {
                if (ParseJsonToC<decltype(p.display_name), _Account>(p.display_name, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "duration")
            {
                if (ParseJsonToC<decltype(p.duration), _Account>(p.duration, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "file_count")
            {
                if (ParseJsonToC<decltype(p.file_count), _Account>(p.file_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "folder_count")
            {
                if (ParseJsonToC<decltype(p.folder_count), _Account>(p.folder_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "frames")
            {
                if (ParseJsonToC<decltype(p.frames), _Account>(p.frames, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _Account>(p.id, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "image")
            {
                if (ParseJsonToC<decltype(p.image), _Account>(p.image, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "image_128")
            {
                if (ParseJsonToC<decltype(p.image_128), _Account>(p.image_128, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "image_256")
            {
                if (ParseJsonToC<decltype(p.image_256), _Account>(p.image_256, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "image_32")
            {
                if (ParseJsonToC<decltype(p.image_32), _Account>(p.image_32, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "image_64")
            {
                if (ParseJsonToC<decltype(p.image_64), _Account>(p.image_64, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "inserted_at")
            {
                if (ParseJsonToC<decltype(p.inserted_at), _Account>(p.inserted_at, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "invoice_emails")
            {
                if (ParseJsonToC<decltype(p.invoice_emails), _Account>(p.invoice_emails, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "lifetime_file_count")
            {
                if (ParseJsonToC<decltype(p.lifetime_file_count), _Account>(p.lifetime_file_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "line1")
            {
                if (ParseJsonToC<decltype(p.line1), _Account>(p.line1, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "line2")
            {
                if (ParseJsonToC<decltype(p.line2), _Account>(p.line2, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "locked_at")
            {
                if (ParseJsonToC<decltype(p.locked_at), _Account>(p.locked_at, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "member_count")
            {
                if (ParseJsonToC<decltype(p.member_count), _Account>(p.member_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "mfa_enforced_at")
            {
                if (ParseJsonToC<decltype(p.mfa_enforced_at), _Account>(p.mfa_enforced_at, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "offline_config")
            {
                if (ParseJsonToC<decltype(p.offline_config), _Account>(p.offline_config, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "owner")
            {
                if (ParseJsonToC<decltype(p.owner), _Account>(p.owner, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "owner_id")
            {
                if (ParseJsonToC<decltype(p.owner_id), _Account>(p.owner_id, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "plan")
            {
                if (ParseJsonToC<decltype(p.plan), _Account>(p.plan, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "postal_code")
            {
                if (ParseJsonToC<decltype(p.postal_code), _Account>(p.postal_code, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "project_count")
            {
                if (ParseJsonToC<decltype(p.project_count), _Account>(p.project_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "resource_id")
            {
                if (ParseJsonToC<decltype(p.resource_id), _Account>(p.resource_id, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "revenue_reported_at")
            {
                if (ParseJsonToC<decltype(p.revenue_reported_at), _Account>(p.revenue_reported_at, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "reviewer_count")
            {
                if (ParseJsonToC<decltype(p.reviewer_count), _Account>(p.reviewer_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "state")
            {
                if (ParseJsonToC<decltype(p.state), _Account>(p.state, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "storage")
            {
                if (ParseJsonToC<decltype(p.storage), _Account>(p.storage, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "subscription")
            {
                if (ParseJsonToC<decltype(p.subscription), _Account>(p.subscription, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "team_count")
            {
                if (ParseJsonToC<decltype(p.team_count), _Account>(p.team_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "unpaid_at")
            {
                if (ParseJsonToC<decltype(p.unpaid_at), _Account>(p.unpaid_at, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "updated_at")
            {
                if (ParseJsonToC<decltype(p.updated_at), _Account>(p.updated_at, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "upload_url")
            {
                if (ParseJsonToC<decltype(p.upload_url), _Account>(p.upload_url, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "user_count")
            {
                if (ParseJsonToC<decltype(p.user_count), _Account>(p.user_count, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "vat")
            {
                if (ParseJsonToC<decltype(p.vat), _Account>(p.vat, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "version")
            {
                if (ParseJsonToC<decltype(p.version), _Account>(p.version, p, json_, i))
                    continue;
            }
        }
    }


    void to_json(json& j, const _data_points& p)
    {
        j = json{ {"_type", p._type},
                  {"order", p.order},
                  {"value", p.value} };
    }
    void from_json(const json& jj, _data_points& p) {

        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _data_points>(p._type, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "order")
            {
                if (ParseJsonToC<decltype(p.order), _data_points>(p.order, p, json_, i))
                    continue;
            }
            else if (p.Parameters[i] == "value")
            {
                if (ParseJsonToC<decltype(p.value), _data_points>(p.value, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _watermark_blocks& p)
    {
        j = json{ {"_type", p._type},
                  {"alpha", p.alpha},
                  {"data_points", p.data_points},
                  {"font_size", p.font_size},
                  {"name", p.name},
                  {"position", p.position},
                  {"position_reference_point", p.position_reference_point},
                  {"position_x", p.position_x},
                  {"position_y", p.position_y},
                  {"scroll_text", p.scroll_text},
                  {"text_alignment", p.text_alignment},
                  {"text_color", p.text_color},
                  {"text_shadow", p.text_shadow}
                };
    }
    void from_json(const json& jj, _watermark_blocks& p)
    {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _watermark_blocks>(p._type, p, json_, i))
                    continue;
            }

            if (p.Parameters[i] == "alpha")
            {
                if (ParseJsonToC<decltype(p.alpha), _watermark_blocks>(p.alpha, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "data_points")
            {
                if (ParseJsonToC<decltype(p.data_points), _watermark_blocks>(p.data_points, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "font_size")
            {
                if (ParseJsonToC<decltype(p.data_points), _watermark_blocks>(p.data_points, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name")
            {
                if (ParseJsonToC<decltype(p.data_points), _watermark_blocks>(p.data_points, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "position")
            {
                if (ParseJsonToC<decltype(p.data_points), _watermark_blocks>(p.data_points, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "position_reference_point")
            {
                if (ParseJsonToC<decltype(p.position_reference_point), _watermark_blocks>(p.position_reference_point, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "position_x")
            {
                if (ParseJsonToC<decltype(p.position_x), _watermark_blocks>(p.position_x, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "position_y")
            {
                if (ParseJsonToC<decltype(p.position_y), _watermark_blocks>(p.position_y, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "scroll_text")
            {
                if (ParseJsonToC<decltype(p.scroll_text), _watermark_blocks>(p.scroll_text, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "text_alignment")
            {
                if (ParseJsonToC<decltype(p.text_alignment), _watermark_blocks>(p.text_alignment, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "text_color")
            {
                if (ParseJsonToC<decltype(p.text_color), _watermark_blocks>(p.text_color, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "text_shadow")
            {
                if (ParseJsonToC<decltype(p.text_shadow), _watermark_blocks>(p.text_shadow, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const  _session_watermark_template& p)
    {
        j = json{ {"_type", p._type},
                  {"account_id", p.account_id},
                  {"app_default", p.app_default},
                  {"creator_id", p.creator_id},
                  {"id", p.id},
                  {"name", p.name},
                  {"default_session_watermark_template", p.default_session_watermark_template} 
               };
    }
    void from_json(const json& jj, _session_watermark_template& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _session_watermark_template>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account_id")
            {
                if (ParseJsonToC<decltype(p.account_id), _session_watermark_template>(p.account_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "app_default")
            {
                if (ParseJsonToC<decltype(p.app_default), _session_watermark_template>(p.app_default, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "creator_id")
            {
                if (ParseJsonToC<decltype(p.creator_id), _session_watermark_template>(p.creator_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _session_watermark_template>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name")
            {
                if (ParseJsonToC<decltype(p.name), _session_watermark_template>(p.name, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "default_session_watermark_template")
            {
                if (ParseJsonToC<decltype(p.default_session_watermark_template), _session_watermark_template>(p.default_session_watermark_template, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const  _admin_only_actions& p)
    {
        j = json{ {"_type", p._type},
                  {"account_id", p.lifecycle}
        };
    }
    void from_json(const json& jj, _admin_only_actions& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _admin_only_actions>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "lifecycle")
            {
                if (ParseJsonToC<decltype(p.lifecycle), _admin_only_actions>(p.lifecycle, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _text& p) {
        j = json{ {"_type", p._type},
                  {"account_id", p.alpha},
                  {"lines", p.lines},
                  {"position", p.position}};
    }
    void from_json(const json& jj, _text& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _text>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "alpha")
            {
                if (ParseJsonToC<decltype(p.alpha), _text>(p.alpha, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "position")
            {
                if (ParseJsonToC<decltype(p.position), _text>(p.position, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account_id")
            {
                if (ParseJsonToC<decltype(p.lines), _text>(p.lines, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _imageInfo& p) {
        j = json{ {"_type", p._type},
                  {"bucket", p.bucket},
                  {"key", p.key},
                  {"uploaded", p.uploaded}
                };
    }
    void from_json(const json& jj, _imageInfo& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _imageInfo>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "bucket")
            {
                if (ParseJsonToC<decltype(p.bucket), _imageInfo>(p.bucket, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "key")
            {
                if (ParseJsonToC<decltype(p.key), _imageInfo>(p.key, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "uploaded")
            {
                if (ParseJsonToC<decltype(p.uploaded), _imageInfo>(p.uploaded, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _image& p)
    {
        j = json{ {"_type", p._type},
                  {"alpha", p.alpha},
                  {"imageInfo", p.imageInfo},
                  {"position", p.position}
        };
    }
    void from_json(const json& jj, _image& p) {

        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _image>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "alpha")
            {
                if (ParseJsonToC<decltype(p.alpha), _image>(p.alpha, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "imageInfo")
            {
                if (ParseJsonToC<decltype(p.imageInfo), _image>(p.imageInfo, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "position")
            {
                if (ParseJsonToC<decltype(p.position), _image>(p.position, p, json_, i))
                    continue;
            }
        }
    }
    void to_json(json& j, const  _watermark& p) {
        j = json{ {"_type", p._type},
                  {"image", p.image},
                  {"text", p.text}
                };
    }

    void from_json(const json& jj, _watermark& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _watermark>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image")
            {
                if (ParseJsonToC<decltype(p.image), _watermark>(p.image, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "text")
            {
                if (ParseJsonToC<decltype(p.text), _watermark>(p.text, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const  _email_branding& p) {
        j = json{ {"_type", p._type},
                  {"image", p.image},
                  {"accent_color", p.accent_color},
                  {"background_color", p.background_color}
                };
    }
    void from_json(const json& jj, _email_branding& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _email_branding>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image")
            {
                if (ParseJsonToC<decltype(p.image), _email_branding>(p.image, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "accent_color")
            {
                if (ParseJsonToC<decltype(p.accent_color), _email_branding>(p.accent_color, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "background_color")
            {
                if (ParseJsonToC<decltype(p.background_color), _email_branding>(p.background_color, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const  _teams& p) {
        j = json{ {"_type", p._type},
        {"account_id", p.account_id},
        {"disable_sbwm_internally", p.disable_sbwm_internally},
        {"creator_id", p.creator_id},
        {"storage", p.storage},
        {"default_font_color", p.default_font_color},
        {"access", p.access},
        {"font_color", p.font_color},
        //    "user_role" : { },
        {"file_count", p.file_count},
        {"default_session_watermark_template", p.default_session_watermark_template},
        {"link", p.link},
        {"asset_lifecycle_policy", p.asset_lifecycle_policy},
        {"upload_url", p.upload_url},
        {"admin_only_actions", p.admin_only_actions},
        {"deleted_at", p.deleted_at},
        {"default_background_color", p.default_background_color},
        {"storage_limit", p.storage_limit},
        {"location", p.location},
        {"name", p.name},
        {"default_color", p.default_color},
        //    "slack_webhook" : { },
        {"image_64", p.image_64},
        {"archived_storage", p.archived_storage},
        {"image_128", p.image_128},
        {"solo", p.solo},
        {"account", p.account},
        {"image_32", p.image_32},
        {"inserted_at", p.inserted_at},
        {"updated_at", p.updated_at},
        {"tproject_count", p.tproject_count},
        {"duration", p.duration},
        {"team_image", p.team_image},
        {"collaborator_count", p.collaborator_count},
        {"bio", p.bio},
        {"watermark", p.watermark},
        {"color", p.color},
        {"email_branding", p.email_branding},
        {"folder_count", p.folder_count},
        {"dark_theme", p.dark_theme},
        {"default_session_watermark_template_id", p.default_session_watermark_template_id},
        {"session_watermark_templates", p.session_watermark_templates},
        {"id", p.id},
        {"member_limit", p.member_limit},
        {"frames", p.frames},
        {"member_count", p.member_count},
        {"image_256", p.image_256},
        {"background_color", p.background_color}
        };
    }
    void from_json(const json& jj, _teams& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _teams>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account_id")
            {
                if (ParseJsonToC<decltype(p.account_id), _teams>(p.account_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "disable_sbwm_internally")
            {
                if (ParseJsonToC<decltype(p.disable_sbwm_internally), _teams>(p.disable_sbwm_internally, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "creator_id")
            {
                if (ParseJsonToC<decltype(p.creator_id), _teams>(p.creator_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "storage")
            {
                if (ParseJsonToC<decltype(p.storage), _teams>(p.storage, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "default_font_color")
            {
                if (ParseJsonToC<decltype(p.default_font_color), _teams>(p.default_font_color, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "access")
            {
                if (ParseJsonToC<decltype(p.access), _teams>(p.access, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "font_color")
            {
                if (ParseJsonToC<decltype(p.font_color), _teams>(p.font_color, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "file_count")
            {
                if (ParseJsonToC<decltype(p.file_count), _teams>(p.file_count, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "default_session_watermark_template")
            {
                if (ParseJsonToC<decltype(p.default_session_watermark_template), _teams>(p.default_session_watermark_template, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "link")
            {
                if (ParseJsonToC<decltype(p.link), _teams>(p.link, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "asset_lifecycle_policy")
            {
                if (ParseJsonToC<decltype(p.asset_lifecycle_policy), _teams>(p.asset_lifecycle_policy, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "upload_url")
            {
                if (ParseJsonToC<decltype(p.upload_url), _teams>(p.upload_url, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "admin_only_actions")
            {
                if (ParseJsonToC<decltype(p.admin_only_actions), _teams>(p.admin_only_actions, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "deleted_at")
            {
                if (ParseJsonToC<decltype(p.deleted_at), _teams>(p.deleted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "default_background_color")
            {
                if (ParseJsonToC<decltype(p.default_background_color), _teams>(p.default_background_color, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "storage_limit")
            {
                if (ParseJsonToC<decltype(p.storage_limit), _teams>(p.storage_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "location")
            {
                if (ParseJsonToC<decltype(p.location), _teams>(p.location, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name")
            {
                if (ParseJsonToC<decltype(p.name), _teams>(p.name, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "default_color")
            {
                if (ParseJsonToC<decltype(p.default_color), _teams>(p.default_color, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_64")
            {
                if (ParseJsonToC<decltype(p.image_64), _teams>(p.image_64, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "archived_storage")
            {
                if (ParseJsonToC<decltype(p.archived_storage), _teams>(p.archived_storage, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_128")
            {
                if (ParseJsonToC<decltype(p.image_128), _teams>(p.image_128, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "solo")
            {
                if (ParseJsonToC<decltype(p.solo), _teams>(p.solo, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account")
            {
                if (ParseJsonToC<decltype(p.account), _teams>(p.account, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_32")
            {
                if (ParseJsonToC<decltype(p.image_32), _teams>(p.image_32, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "inserted_at")
            {
                if (ParseJsonToC<decltype(p.inserted_at), _teams>(p.inserted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "updated_at")
            {
                if (ParseJsonToC<decltype(p.updated_at), _teams>(p.updated_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "tproject_count")
            {
                if (ParseJsonToC<decltype(p.tproject_count), _teams>(p.tproject_count, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "duration")
            {
                if (ParseJsonToC<decltype(p.duration), _teams>(p.duration, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "team_image")
            {
                if (ParseJsonToC<decltype(p.team_image), _teams>(p.team_image, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "collaborator_count")
            {
                if (ParseJsonToC<decltype(p.collaborator_count), _teams>(p.collaborator_count, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "bio")
            {
                if (ParseJsonToC<decltype(p.bio), _teams>(p.bio, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "watermark")
            {
                if (ParseJsonToC<decltype(p.watermark), _teams>(p.watermark, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "color")
            {
                if (ParseJsonToC<decltype(p.color), _teams>(p.color, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "email_branding")
            {
                if (ParseJsonToC<decltype(p.email_branding), _teams>(p.email_branding, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "folder_count")
            {
                if (ParseJsonToC<decltype(p.folder_count), _teams>(p.folder_count, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "dark_theme")
            {
                if (ParseJsonToC<decltype(p.dark_theme), _teams>(p.dark_theme, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "default_session_watermark_template_id")
            {
                if (ParseJsonToC<decltype(p.default_session_watermark_template_id), _teams>(p.default_session_watermark_template_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "session_watermark_templates")
            {
                if (ParseJsonToC<decltype(p.session_watermark_templates), _teams>(p.session_watermark_templates, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _teams>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "member_limit")
            {
                if (ParseJsonToC<decltype(p.member_limit), _teams>(p.member_limit, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "frames")
            {
                if (ParseJsonToC<decltype(p.frames), _teams>(p.frames, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "member_count")
            {
                if (ParseJsonToC<decltype(p.member_count), _teams>(p.member_count, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_256")
            {
                if (ParseJsonToC<decltype(p.image_256), _teams>(p.image_256, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "background_color")
            {
                if (ParseJsonToC<decltype(p.background_color), _teams>(p.background_color, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const  _project_preferences& p) {
        j = json{ {"_type", p._type},
                  {"id", p.id},
                  {"collaborator_can_download", p.collaborator_can_download},
                  {"collaborator_can_invite", p.collaborator_can_invite},
                  {"collaborator_can_share", p.collaborator_can_share},
                  {"deleted_at", p.deleted_at},
                  {"devices_enabled", p.devices_enabled},
                  {"inserted_at", p.inserted_at},
                  {"notify_on_new_asset", p.notify_on_new_asset},
                  {"notify_on_new_collaborator", p.notify_on_new_collaborator},
                  {"notify_on_new_comment", p.notify_on_new_comment},
                  {"notify_on_new_mention", p.notify_on_new_mention},
                  {"notify_on_updated_label", p.notify_on_updated_label},
                  {"notify_slack", p.notify_slack},
                  {"project_id", p.project_id},
                  {"updated_at", p.updated_at},
                  {"user_id", p.user_id} };
    }
    void from_json(const json& jj, _project_preferences& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _project_preferences>(p._type, p, json_, i))
                    continue;
            }

            if (p.Parameters[i] == "collaborator_can_download")
            {
                if (ParseJsonToC<decltype(p.collaborator_can_download), _project_preferences>(p.collaborator_can_download, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "collaborator_can_invite")
            {
                if (ParseJsonToC<decltype(p.collaborator_can_invite), _project_preferences>(p.collaborator_can_invite, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "collaborator_can_share")
            {
                if (ParseJsonToC<decltype(p.collaborator_can_share), _project_preferences>(p.collaborator_can_share, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "deleted_at")
            {
                if (ParseJsonToC<decltype(p.deleted_at), _project_preferences>(p.deleted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "devices_enabled")
            {
                if (ParseJsonToC<decltype(p.devices_enabled), _project_preferences>(p.devices_enabled, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _project_preferences>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "inserted_at")
            {
                if (ParseJsonToC<decltype(p.inserted_at), _project_preferences>(p.inserted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "notify_on_new_asset")
            {
                if (ParseJsonToC<decltype(p.notify_on_new_asset), _project_preferences>(p.notify_on_new_asset, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "notify_on_new_collaborator")
            {
                if (ParseJsonToC<decltype(p.notify_on_new_collaborator), _project_preferences>(p.notify_on_new_collaborator, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "notify_on_new_comment")
            {
                if (ParseJsonToC<decltype(p.notify_on_new_comment), _project_preferences>(p.notify_on_new_comment, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "notify_on_new_mention")
            {
                if (ParseJsonToC<decltype(p.notify_on_new_mention), _project_preferences>(p.notify_on_new_mention, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "notify_on_updated_label")
            {
                if (ParseJsonToC<decltype(p.notify_on_updated_label), _project_preferences>(p.notify_on_updated_label, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "notify_slack")
            {
                if (ParseJsonToC<decltype(p.notify_slack), _project_preferences>(p.notify_slack, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "project_id")
            {
                if (ParseJsonToC<decltype(p.project_id), _project_preferences>(p.project_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "updated_at")
            {
                if (ParseJsonToC<decltype(p.updated_at), _project_preferences>(p.updated_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_id")
            {
                if (ParseJsonToC<decltype(p.user_id), _project_preferences>(p.user_id, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const  _Project& p) {
        j = json{ {"_type", p._type},
                  {"id", p.id},
                  {"name", p.name},
                  {"owner_id", p.owner_id},
                  {"_private", p.Private},
                  {"project_preferences", p.project_preferences},
                  {"root_asset_id", p.root_asset_id},
                  {"team_id", p.team_id} };
    }
    void from_json(const json& jj, _Project& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _Project>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _Project>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name")
            {
                if (ParseJsonToC<decltype(p.name), _Project>(p.name, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "owner_id")
            {
                if (ParseJsonToC<decltype(p.owner_id), _Project>(p.owner_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "private")
            {
                if (ParseJsonToC<decltype(p.Private), _Project>(p.Private, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "project_preferences")
            {
                if (ParseJsonToC<decltype(p.project_preferences), _Project>(p.project_preferences, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "root_asset_id")
            {
                if (ParseJsonToC<decltype(p.root_asset_id), _Project>(p.root_asset_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "team_id")
            {
                if (ParseJsonToC<decltype(p.team_id), _Project>(p.team_id, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const  _Roles& p){
        j = json{ {"_type", p._type},
                  {"admin", p.admin},
                  {"id", p.id},
                  {"sales", p.sales},
                  {"support", p.support} };
    }
    void from_json(const json& jj, _Roles& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _Roles>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "admin")
            {
                if (ParseJsonToC<decltype(p.admin), _Roles>(p.admin, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _Roles>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "sales")
            {
                if (ParseJsonToC<decltype(p.sales), _Roles>(p.sales, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "support")
            {
                if (ParseJsonToC<decltype(p.support), _Roles>(p.support, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const  _Creator& p) {
        j = json{ {"_type", p._type},
                   {"digest_frequency"},
                   {"image_32"},
                   {"image_128"},
                   {"from_google"},
                   {"mfa_enforced_at"},
                   {"email"},
                   {"name"},
                   {"image_64"},
                   {"timezone_value"},
                   {"account_id"},
                   {"updated_at"},
                   {"image_256"},
                   {"user_hash"},
                   {"upload_url"},
                   {"profile_image"},
                   {"first_login_at"},
                   {"joined_via"},
                   {"id"},
                   {"next_digest_date"},
                   {"last_seen"},
                   {"inserted_at"},
                   {"roles"},
                   {"user_default_color"}};
    }
    void from_json(const json& jj, _Creator& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _Creator>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "digest_frequency")
            {
                if (ParseJsonToC<decltype(p.digest_frequency), _Creator>(p.digest_frequency, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_32")
            {
                if (ParseJsonToC<decltype(p.image_32), _Creator>(p.image_32, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_128")
            {
                if (ParseJsonToC<decltype(p.image_128), _Creator>(p.image_128, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "from_google")
            {
                if (ParseJsonToC<decltype(p.from_google), _Creator>(p.from_google, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "mfa_enforced_at")
            {
                if (ParseJsonToC<decltype(p.mfa_enforced_at), _Creator>(p.mfa_enforced_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "email")
            {
                if (ParseJsonToC<decltype(p.email), _Creator>(p.email, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name")
            {
                if (ParseJsonToC<decltype(p.name), _Creator>(p.name, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_64")
            {
                if (ParseJsonToC<decltype(p.image_64), _Creator>(p.image_64, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "timezone_value")
            {
                if (ParseJsonToC<decltype(p.timezone_value), _Creator>(p.timezone_value, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account_id")
            {
                if (ParseJsonToC<decltype(p.account_id), _Creator>(p.account_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "updated_at")
            {
                if (ParseJsonToC<decltype(p.updated_at), _Creator>(p.updated_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_256")
            {
                if (ParseJsonToC<decltype(p.image_256), _Creator>(p.image_256, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_hash")
            {
                if (ParseJsonToC<decltype(p.user_hash), _Creator>(p.user_hash, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "upload_url")
            {
                if (ParseJsonToC<decltype(p.upload_url), _Creator>(p.upload_url, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "profile_image")
            {
                if (ParseJsonToC<decltype(p.profile_image), _Creator>(p.profile_image, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "first_login_at")
            {
                if (ParseJsonToC<decltype(p.first_login_at), _Creator>(p.first_login_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "joined_via")
            {
                if (ParseJsonToC<decltype(p.joined_via), _Creator>(p.joined_via, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _Creator>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "next_digest_date")
            {
                if (ParseJsonToC<decltype(p.next_digest_date), _Creator>(p.next_digest_date, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "last_seen")
            {
                if (ParseJsonToC<decltype(p.last_seen), _Creator>(p.last_seen, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "inserted_at")
            {
                if (ParseJsonToC<decltype(p.inserted_at), _Creator>(p.inserted_at, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "roles")
            {
                if (ParseJsonToC<decltype(p.roles), _Creator>(p.roles, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_default_color")
            {
                if (ParseJsonToC<decltype(p.user_default_color), _Creator>(p.user_default_color, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _required_transcodes& p) {
        j = json{ {"_type", p._type},
                  {"cover", p.cover},
                  {"finalized", p.finalized},
                  {"h264_1080_best", p.h264_1080_best},
                  {"h264_2160", p.h264_2160},
                  {"h264_360", p.h264_360},
                  {"h264_540", p.h264_540},
                  {"h264_720", p.h264_720},
                  {"image_full", p.image_full},
                  {"image_high", p.image_high},
                  {"page_proxy", p.page_proxy},
                  {"thumb", p.thumb},
                  {"thumb_540", p.thumb_540},
                  {"thumb_orig_ar_540", p.thumb_orig_ar_540},
                  {"thumb_scrub", p.thumb_scrub} };
    }
    void from_json(const json& jj, _required_transcodes& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _required_transcodes>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "cover")
            {
                if (ParseJsonToC<decltype(p.cover), _required_transcodes>(p.cover, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "finalized")
            {
                if (ParseJsonToC<decltype(p.finalized), _required_transcodes>(p.finalized, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "h264_1080_best")
            {
                if (ParseJsonToC<decltype(p.h264_1080_best), _required_transcodes>(p.h264_1080_best, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "h264_2160")
            {
                if (ParseJsonToC<decltype(p.h264_2160), _required_transcodes>(p.h264_2160, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "h264_360")
            {
                if (ParseJsonToC<decltype(p.h264_360), _required_transcodes>(p.h264_360, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "h264_540")
            {
                if (ParseJsonToC<decltype(p.h264_540), _required_transcodes>(p.h264_540, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "h264_720")
            {
                if (ParseJsonToC<decltype(p.h264_720), _required_transcodes>(p.h264_720, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_full")
            {
                if (ParseJsonToC<decltype(p.image_full), _required_transcodes>(p.image_full, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "image_high")
            {
                if (ParseJsonToC<decltype(p.image_high), _required_transcodes>(p.image_high, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "page_proxy")
            {
                if (ParseJsonToC<decltype(p.page_proxy), _required_transcodes>(p.page_proxy, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "thumb")
            {
                if (ParseJsonToC<decltype(p.thumb), _required_transcodes>(p.thumb, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "thumb_540")
            {
                if (ParseJsonToC<decltype(p.thumb_540), _required_transcodes>(p.thumb_540, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "thumb_orig_ar_540")
            {
                if (ParseJsonToC<decltype(p.thumb_orig_ar_540), _required_transcodes>(p.thumb_orig_ar_540, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "thumb_scrub")
            {
                if (ParseJsonToC<decltype(p.thumb_scrub), _required_transcodes>(p.thumb_scrub, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _user_permissions& p) {
        j = json{ {"_type", p._type},
                  {"can_download", p.can_download},
                  {"can_modify_template", p.can_modify_template},
                  {"can_public_share_presentation", p.can_public_share_presentation},
                  {"can_public_share_review_link", p.can_public_share_review_link},
                  {"can_share_downloadable_presentation", p.can_share_downloadable_presentation},
                  {"can_share_downloadable_review_link", p.can_share_downloadable_review_link},
                  {"can_share_unwatermarked_presentation", p.can_share_unwatermarked_presentation},
                  {"can_share_unwatermarked_review_link", p.can_share_unwatermarked_review_link} };
    }
    void from_json(const json& jj, _user_permissions& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _user_permissions>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "can_download")
            {
                if (ParseJsonToC<decltype(p.can_download), _user_permissions>(p.can_download, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "can_modify_template")
            {
                if (ParseJsonToC<decltype(p.can_modify_template), _user_permissions>(p.can_modify_template, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "can_public_share_presentation")
            {
                if (ParseJsonToC<decltype(p.can_public_share_presentation), _user_permissions>(p.can_public_share_presentation, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "can_public_share_review_link")
            {
                if (ParseJsonToC<decltype(p.can_public_share_review_link), _user_permissions>(p.can_public_share_review_link, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "can_share_downloadable_presentation")
            {
                if (ParseJsonToC<decltype(p.can_share_downloadable_presentation), _user_permissions>(p.can_share_downloadable_presentation, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "can_share_downloadable_review_link")
            {
                if (ParseJsonToC<decltype(p.can_share_downloadable_review_link), _user_permissions>(p.can_share_downloadable_review_link, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "can_share_unwatermarked_presentation")
            {
                if (ParseJsonToC<decltype(p.can_share_unwatermarked_presentation), _user_permissions>(p.can_share_unwatermarked_presentation, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "can_share_unwatermarked_review_link")
            {
                if (ParseJsonToC<decltype(p.can_share_unwatermarked_review_link), _user_permissions>(p.can_share_unwatermarked_review_link, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _Assest& p) {
        j = json{ {"_type", p._type},
              {"account_id", p.account_id},
              {"asset_type", p.asset_type},
              {"archive_from", p.archive_from},
              {"bundle", p.bundle},
              {"bundle_view", p.bundle_view},
              {"cover_asset_id", p.cover_asset_id},
              {"creator", p.creator},
              {"id", p.id},
              {"index", p.index},
              {"is_bundle_child", p.is_bundle_child},
              {"is_hls_required", p.is_hls_required},
              {"is_session_watermarked", p.is_session_watermarked},
              {"item_count", p.item_count},
              {"label", p.label},
              {"name", p.name},
              {"original", p.original},
              {"project_id", p.project_id},
              {"required_transcodes", p.required_transcodes},
              {"team_id", p.team_id},
              {"user_permissions", p.user_permissions},
              {"type", p.type} };
    }
    void from_json(const json& jj, _Assest& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _Assest>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account_id")
            {
                if (ParseJsonToC<decltype(p.account_id), _Assest>(p.account_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "asset_type")
            {
                if (ParseJsonToC<decltype(p.asset_type), _Assest>(p.asset_type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "archive_from")
            {
                if (ParseJsonToC<decltype(p.archive_from), _Assest>(p.archive_from, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "bundle")
            {
                if (ParseJsonToC<decltype(p.bundle), _Assest>(p.bundle, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "bundle_view")
            {
                if (ParseJsonToC<decltype(p.bundle_view), _Assest>(p.bundle_view, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "cover_asset_id")
            {
                if (ParseJsonToC<decltype(p.cover_asset_id), _Assest>(p.cover_asset_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "creator")
            {
                if (ParseJsonToC<decltype(p.creator), _Assest>(p.creator, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _Assest>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "index")
            {
                if (ParseJsonToC<decltype(p.index), _Assest>(p.index, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "is_bundle_child")
            {
                if (ParseJsonToC<decltype(p.is_bundle_child), _Assest>(p.is_bundle_child, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "is_hls_required")
            {
                if (ParseJsonToC<decltype(p.is_hls_required), _Assest>(p.is_hls_required, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "is_session_watermarked")
            {
                if (ParseJsonToC<decltype(p.is_session_watermarked), _Assest>(p.is_session_watermarked, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "item_count")
            {
                if (ParseJsonToC<decltype(p.item_count), _Assest>(p.item_count, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "label")
            {
                if (ParseJsonToC<decltype(p.label), _Assest>(p.label, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name")
            {
                if (ParseJsonToC<decltype(p.name), _Assest>(p.name, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "original")
            {
                if (ParseJsonToC<decltype(p.original), _Assest>(p.original, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "project_id")
            {
                if (ParseJsonToC<decltype(p.project_id), _Assest>(p.project_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "required_transcodes")
            {
                if (ParseJsonToC<decltype(p.required_transcodes), _Assest>(p.required_transcodes, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "team_id")
            {
                if (ParseJsonToC<decltype(p.team_id), _Assest>(p.team_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "user_permissions")
            {
                if (ParseJsonToC<decltype(p.user_permissions), _Assest>(p.user_permissions, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "type")
            {
                if (ParseJsonToC<decltype(p.type), _Assest>(p.type, p, json_, i))
                    continue;
            }
        }
    }

    void to_json(json& j, const _User& p) {
        j = json{ {"_type", p._type},
                  {"account_id", p.account_id},
                  {"email", p.email},
                  {"id", p.id},
                  {"name", p.name},
                  {"roles", p.roles} };
    }
    void from_json(const json& jj, _User& p) {
        // we need to save the json value in non cosnt variable since we will update the json with paramerters values
        json json_ = jj;
        // get the parameters that we need to convert into C++ code 
        if (json_.at("Parameters").type() != nlohmann::detail::value_t::null) {
            json_.at("Parameters").get_to(p.Parameters);
        }

        if (!p.CheckVariablesExistence(p.Parameters))
        {
            return;
        }

        int count = p.Parameters.size();
        for (int i = 0; i < count; i++)
        {
            if (p.Parameters[i] == "_type")
            {
                if (ParseJsonToC<decltype(p._type), _User>(p._type, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "account_id")
            {
                if (ParseJsonToC<decltype(p.account_id), _User>(p.account_id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "email")
            {
                if (ParseJsonToC<decltype(p.email), _User>(p.email, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "id")
            {
                if (ParseJsonToC<decltype(p.id), _User>(p.id, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "name")
            {
                if (ParseJsonToC<decltype(p.name), _User>(p.name, p, json_, i))
                    continue;
            }
            if (p.Parameters[i] == "roles")
            {
                if (ParseJsonToC<decltype(p.roles), _User>(p.roles, p, json_, i))
                    continue;
            }

        }
    }
}