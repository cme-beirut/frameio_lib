#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <cmath>

#include <curl/curl.h>
#include <nlohmann/json.hpp>

using json = nlohmann::json;

typedef enum _Request_Type {
	GET,
	POST,
}Request_Type;

class HttpWrapper
{
public :
	HttpWrapper();
	HttpWrapper(std::string my_token);
	bool api_call(json& response_json, std::string endpoint, Request_Type method, std::string payload);
	void Upload_asset_handler(json& response_json, json& asset_creation_response);

private:

	const std::string MY_TOKEN;
	std::vector<int> Calculate_chunks(int total_size, int chunk_count);
	static size_t WriteCallback(void* contents, size_t size, size_t nmemb, void* userp);
	static std::size_t Writefunc(const char* in, std::size_t size, std::size_t num, std::string* out); // utility function for callback of request
	std::size_t  Read_callback(char* ptr, size_t size, size_t nmemb, void* stream);
	void Upload_api_call(std::string host, std::string file_path, std::string asset_filetype, int chunck_size);

};