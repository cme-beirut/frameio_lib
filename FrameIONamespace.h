#pragma once
#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <nlohmann/json.hpp>

#ifndef FRAMEIONAMESAPSE_H
#define FRAMEIONAMESAPSE_H

using namespace std;
using json = nlohmann::json;

namespace FrameIONamespace {
    
    bool CheckVariablesExistenceInStructure(vector<string> VariablesToCheck, vector<string> StructureVariables);

    typedef struct __Authorization {
        vector<string> Parameters;
        string _type;  //: "user",
        string account_id;  // : "95c5cf50-af7f-4c58-948e-0b5519b07880",
        nullptr_t bio;  // : null,
        nullptr_t context;  // : null,
        nullptr_t deleted_at;  // : null,
        string digest_frequency;  // : "0 * * * *",
        string email;  // : "austin.boyle@atomos.com",
        nullptr_t email_confirm_by;  // : null,
        nullptr_t email_preferences;  // : null,
        nullptr_t features_seen;  // : null,
        string first_login_at;  // : "2021-11-03T21:39:21.338000Z",
        bool from_google;  // : false,
        nullptr_t highest_account_role;  // : null,
        string id;  // : "c623173a-dcf1-45ac-a1ae-c6ee8e9bc966",
        nullptr_t image_128;  // : null,
        nullptr_t image_256;  // : null,
        nullptr_t image_32;  // : null,
        nullptr_t image_64;  // : null,
        string inserted_at;  // : "2021-11-03T21:38:25.728019Z",
        string joined_via;  // : "organic",
        string last_seen;  // : "2021-11-16T14:09:32.838248Z",
        nullptr_t location;  // : null,
        nullptr_t mfa_enforced_at;  // : null,
        string name;  // : "Austin Boyle",
        string next_digest_date;  // : "2021-11-03T21:38:25.727877Z",
        nullptr_t phone;  // : null,
        string profile_image;  // : "https://static-assets.frame.io/app/anon.jpg",
        nullptr_t profile_image_original;  // : null,
        nullptr_t roles;  // : null,
        string timezone_value;  // : "America/New_York",
        string updated_at;  // : "2021-11-16T14:09:32.838285Z",
        string upload_url;  // : "https://frameio-uploads-production.s3-accelerate.amazonaws.com/users/c623173a-dcf1-45ac-a1ae-c6ee8e9bc966/profile_image_original.jpg?x-amz-meta-request_id=FrgMIoPyDIXFfvkCag1F&x-amz-meta-resource_id=c623173a-dcf1-45ac-a1ae-c6ee8e9bc966&x-amz-meta-resource_type=user&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAZ5BPIQ3GJVSAXJ5T%2F20211116%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20211116T141006Z&X-Amz-Expires=86400&X-Amz-SignedHeaders=content-type%3Bhost%3Bx-amz-acl&X-Amz-Signature=40b1441b125e5c4f0eaea7bd3b6a526e95cd4a2eba31a1d0b6939393567e4bb6",
        string user_default_color;  // : "#ffef40",
        string user_hash;  // : "3883AED870BB271C22F661D1C4DB094AE83329EEA7E72BDC4A0692049358668D"

        /// <summary>
        /// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
        /// return true if all variables existe or if will return false 
        /// this function is not working for nested variables yet
        /// </summary>
        /// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("account_id");
            StructureVariables.push_back("bio");
            StructureVariables.push_back("context");
            StructureVariables.push_back("deleted_at");
            StructureVariables.push_back("digest_frequency");
            StructureVariables.push_back("email");
            StructureVariables.push_back("email_confirm_by");
            StructureVariables.push_back("email_preferences");
            StructureVariables.push_back("features_seen");
            StructureVariables.push_back("first_login_at");
            StructureVariables.push_back("from_google");
            StructureVariables.push_back("highest_account_role");
            StructureVariables.push_back("id");
            StructureVariables.push_back("image_128");
            StructureVariables.push_back("image_256");
            StructureVariables.push_back("image_32");
            StructureVariables.push_back("image_64");
            StructureVariables.push_back("inserted_at");
            StructureVariables.push_back("joined_via");
            StructureVariables.push_back("last_seen");
            StructureVariables.push_back("location");
            StructureVariables.push_back("mfa_enforced_at");
            StructureVariables.push_back("name");
            StructureVariables.push_back("next_digest_date");
            StructureVariables.push_back("phone");
            StructureVariables.push_back("profile_image");
            StructureVariables.push_back("profile_image_original");
            StructureVariables.push_back("roles");
            StructureVariables.push_back("timezone_value");
            StructureVariables.push_back("updated_at");
            StructureVariables.push_back("upload_url");
            StructureVariables.push_back("user_default_color");
            StructureVariables.push_back("user_hash");
           
#pragma endregion adding variables vector
           
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);

        }

    }_Authorization;

    typedef struct __account_settings {
        vector<string> Parameters;
        string _type; // ": "account_settings",
        bool default_sharing_to_public; // : false,
        string download_min_role; // : "collaborators",
        string id; // : "9c5a9518-6231-42ac-a789-5322ab792468",
        nullptr_t internal_sbwm_template_id; // : null,
        string public_sharing_min_role; // : "team_members",
        bool sbwm_end_user_override; // : false,
        bool sbwm_require_internally; // : false,
        bool sbwm_require_sharing; // : false,
        nullptr_t share_sbwm_template_id; // : null,
        string unwatermarked_internal_access_min_role; // : "admins",
        string unwatermarked_share_min_role; // : "admins"

        /// <summary>
        /// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
        /// return true if all variables existe or if will return false 
        /// this function is not working for nested variables yet
        /// </summary>
        /// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("default_sharing_to_public");
            StructureVariables.push_back("download_min_role");
            StructureVariables.push_back("id");
            StructureVariables.push_back("internal_sbwm_template_id");
            StructureVariables.push_back("public_sharing_min_role");
            StructureVariables.push_back("sbwm_end_user_override");
            StructureVariables.push_back("sbwm_require_internally");
            StructureVariables.push_back("sbwm_require_sharing");
            StructureVariables.push_back("share_sbwm_template_id");
            StructureVariables.push_back("unwatermarked_internal_access_min_role");
            StructureVariables.push_back("unwatermarked_share_min_role");
           
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }

    }_account_settings;

    typedef struct __available_features {
        vector<string> Parameters;
        string _type;
        bool _4k_playback; // true,
        bool archival_storage; // true,
        bool custom_branded_emails; // true,
        bool custom_branded_presentations; // true,
        bool devices; // true,
        bool hdr; // false,
        string id; // "183c85ae-1cf6-44c7-8295-f2eb77d2b411",
        bool password_protected_shares; // true,
        bool private_projects; // true,
        bool reel_player; // true,
        bool secure_sharing; // false,
        bool session_based_watermarking; // false,
        bool share_link_expiration; // true,
        bool team_only_comments; // true

        /// <summary>
        /// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
        /// return true if all variables existe or if will return false 
        /// this function is not working for nested variables yet
        /// </summary>
        /// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_Type");
            StructureVariables.push_back("_4k_playback");
            StructureVariables.push_back("archival_storage");
            StructureVariables.push_back("custom_branded_emails");
            StructureVariables.push_back("custom_branded_presentations");
            StructureVariables.push_back("devices");
            StructureVariables.push_back("hdr");
            StructureVariables.push_back("id");
            StructureVariables.push_back("password_protected_shares");
            StructureVariables.push_back("private_projects");
            StructureVariables.push_back("reel_player");
            StructureVariables.push_back("secure_sharing");
            StructureVariables.push_back("session_based_watermarking");
            StructureVariables.push_back("share_link_expiration");
            StructureVariables.push_back("team_only_comments");

#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_available_features;

    typedef struct __offline_config {
        vector<string> Parameters;
        nullptr_t asset_expiry;// null,
        string id;//null,
        nullptr_t logged_in_limit;// null

        /// <summary>
        /// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
        /// return true if all variables existe or if will return false 
        /// this function is not working for nested variables yet
        /// </summary>
        /// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("asset_expiry");
            StructureVariables.push_back("id");
            StructureVariables.push_back("logged_in_limit");
          
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_offline_config;

    typedef struct __owner {
        vector<string> Parameters;
        string _type;  //: "user",
        string account_id;  // : "95c5cf50-af7f-4c58-948e-0b5519b07880",
        nullptr_t bio;  // : null,
        nullptr_t context;  // : null,
        nullptr_t deleted_at;  // : null,
        string digest_frequency;  // : "0 * * * *",
        string email;  // : "austin.boyle@atomos.com",
        nullptr_t email_confirm_by;  // : null,
        nullptr_t email_preferences;  // : null,
        nullptr_t features_seen;  // : null,
        string first_login_at;  // : "2021-11-03T21:39:21.338000Z",
        bool from_google;  // : false,
        nullptr_t highest_account_role;  // : null,
        string id;  // : "c623173a-dcf1-45ac-a1ae-c6ee8e9bc966",
        nullptr_t image_128;  // : null,
        nullptr_t image_256;  // : null,
        nullptr_t image_32;  // : null,
        nullptr_t image_64;  // : null,
        string inserted_at;  // : "2021-11-03T21:38:25.728019Z",
        string joined_via;  // : "organic",
        string last_seen;  // : "2021-11-16T14:09:32.838248Z",
        nullptr_t location;  // : null,
        nullptr_t mfa_enforced_at;  // : null,
        string name;  // : "Austin Boyle",
        string next_digest_date;  // : "2021-11-03T21:38:25.727877Z",
        nullptr_t phone;  // : null,
        string profile_image;  // : "https://static-assets.frame.io/app/anon.jpg",
        nullptr_t profile_image_original;  // : null,
        nullptr_t roles;  // : null,
        string timezone_value;  // : "America/New_York",
        string updated_at;  // : "2021-11-16T14:09:32.838285Z",
        string upload_url;  // : "https://frameio-uploads-production.s3-accelerate.amazonaws.com/users/c623173a-dcf1-45ac-a1ae-c6ee8e9bc966/profile_image_original.jpg?x-amz-meta-request_id=FrgMIoPyDIXFfvkCag1F&x-amz-meta-resource_id=c623173a-dcf1-45ac-a1ae-c6ee8e9bc966&x-amz-meta-resource_type=user&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAZ5BPIQ3GJVSAXJ5T%2F20211116%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20211116T141006Z&X-Amz-Expires=86400&X-Amz-SignedHeaders=content-type%3Bhost%3Bx-amz-acl&X-Amz-Signature=40b1441b125e5c4f0eaea7bd3b6a526e95cd4a2eba31a1d0b6939393567e4bb6",
        string user_default_color;  // : "#ffef40",


         /// <summary>
        /// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
        /// return true if all variables existe or if will return false 
        /// this function is not working for nested variables yet
        /// </summary>
        /// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("account_id");
            StructureVariables.push_back("bio");
            StructureVariables.push_back("context");
            StructureVariables.push_back("deleted_at");
            StructureVariables.push_back("digest_frequency");
            StructureVariables.push_back("email");
            StructureVariables.push_back("email_confirm_by");
            StructureVariables.push_back("email_preferences");
            StructureVariables.push_back("features_seen");
            StructureVariables.push_back("first_login_at");
            StructureVariables.push_back("from_google");
            StructureVariables.push_back("highest_account_role");
            StructureVariables.push_back("id");
            StructureVariables.push_back("image_128");
            StructureVariables.push_back("image_256");
            StructureVariables.push_back("image_32");
            StructureVariables.push_back("image_64");
            StructureVariables.push_back("inserted_at");
            StructureVariables.push_back("joined_via");
            StructureVariables.push_back("last_seen");
            StructureVariables.push_back("location");
            StructureVariables.push_back("mfa_enforced_at");
            StructureVariables.push_back("name");
            StructureVariables.push_back("next_digest_date");
            StructureVariables.push_back("phone");
            StructureVariables.push_back("profile_image");
            StructureVariables.push_back("profile_image_original");
            StructureVariables.push_back("roles");
            StructureVariables.push_back("timezone_value");
            StructureVariables.push_back("updated_at");
            StructureVariables.push_back("upload_url"); 
            StructureVariables.push_back("user_default_color");


#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }

    }_owner;

    typedef struct __plan {
        vector<string> Parameters;
        string _type; // "plan",
        float archived_storage_limit; // 1000000000000,
        bool autoscaling; // true,
        _available_features available_features;
        nullptr_t collaborator_limit; // null,
        int cost; // 25,
        bool default_plan; // true,
        nullptr_t deleted_at; // null,
        bool enterprise; //false,
        nullptr_t file_limit; // null,
        string id; //"4c368a22-fabf-4093-972b-52762a6c95ec",
        string inserted_at; // "2020-08-13T13:32:12.307924Z",
        nullptr_t lifetime_file_limit; // null,
        nullptr_t member_limit; // null,
        string name; // "v6-team-mo",
        string payment_method; // "stripe",
        string period; // "monthly",
        int project_limit; // null,
        float storage_limit; // 500000000000,
        int team_limit; // 1,
        string tier; // "team",
        string title; // "Team",
        string updated_at; // "2021-02-12T21:14:37.234486Z",
        int user_limit; // 1,
        int user_max; // 20,
        int version; // 6

                 /// <summary>
        /// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
        /// return true if all variables existe or if will return false 
        /// this function is not working for nested variables yet
        /// </summary>
        /// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("archived_storage_limit");
            StructureVariables.push_back("autoscaling");
            StructureVariables.push_back("available_features");
            StructureVariables.push_back("collaborator_limit");
            StructureVariables.push_back("cost");
            StructureVariables.push_back("default_plan");
            StructureVariables.push_back("deleted_at");
            StructureVariables.push_back("enterprise");
            StructureVariables.push_back("file_limit");
            StructureVariables.push_back("id");
            StructureVariables.push_back("inserted_at");
            StructureVariables.push_back("lifetime_file_limit");
            StructureVariables.push_back("member_limit");
            StructureVariables.push_back("name");
            StructureVariables.push_back("payment_method");
            StructureVariables.push_back("period");
            StructureVariables.push_back("project_limit");
            StructureVariables.push_back("storage_limit");
            StructureVariables.push_back("team_limit");
            StructureVariables.push_back("tier");
            StructureVariables.push_back("title");
            StructureVariables.push_back("updated_at");
            StructureVariables.push_back("user_limit");
            StructureVariables.push_back("user_max");
            StructureVariables.push_back("version");


#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }


    }_plan;

    typedef struct __promotion {
        vector<string> Parameters;
        string _type; // "promotion",
        bool autoscaling; // true,
        bool can_override_limitations; // false,
        nullptr_t deleted_at; // null,
        string expires_at; //  "2050-12-31T00:00:00.000000Z",
        nullptr_t header_subtext; // null,
        nullptr_t header_text; // null,
        string id; // "8e71b8bb-f142-4ab5-9925-c04357abbaa5",
        string inserted_at; // "2020-08-13T19:22:30.275086Z",
        bool is_trial; // true,
        int new_price; // 0,
        bool no_credit_card; // true,
        string plan_id; // "4c368a22-fabf-4093-972b-52762a6c95ec",
        string promo_code; // "V6TM14D",
        nullptr_t submit_text; // null,
        int trial_length; // 14,
        string updated_at; // "2021-09-22T14:25:34.347813Z"

                  /// <summary>
        /// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
        /// return true if all variables existe or if will return false 
        /// this function is not working for nested variables yet
        /// </summary>
        /// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("autoscaling");
            StructureVariables.push_back("can_override_limitations");
            StructureVariables.push_back("deleted_at");
            StructureVariables.push_back("expires_at");
            StructureVariables.push_back("header_subtext");
            StructureVariables.push_back("header_text");
            StructureVariables.push_back("id");
            StructureVariables.push_back("inserted_at");
            StructureVariables.push_back("is_trial");
            StructureVariables.push_back("new_price");
            StructureVariables.push_back("no_credit_card");
            StructureVariables.push_back("plan_id");
            StructureVariables.push_back("promo_code");
            StructureVariables.push_back("submit_text");
            StructureVariables.push_back("trial_length");
            StructureVariables.push_back("updated_at");


#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }


    }_promotion;

    typedef struct __subscription {
        vector<string> Parameters;
        string _type; // "subscription",
        string account_id; // "95c5cf50-af7f-4c58-948e-0b5519b07880",
        int archived_storage_limit; // 0,
        int balance; // 0,
        nullptr_t cancellation_option; // null,
        nullptr_t cancellation_reason; // null,
        nullptr_t cancelled_at; // null,
        nullptr_t deleted_at; // null,
        string id; // "6afd1699-a511-4ea4-9267-0802acc34b5b",
        string inserted_at; // "2021-11-03T21:38:25.739410Z",
        string last_payment_at; // "2021-11-03T21:38:29.574064Z",
        int member_limit; // 0,
        string next_bill_at; // "2021-11-18T21:38:25.000000Z",
        bool on_trial; // true,
        _plan plan;
        string plan_id; // "4c368a22-fabf-4093-972b-52762a6c95ec",
        _promotion promotion;
        nullptr_t promotion_expires_at; // "2021-11-17T21:38:25.978544Z",
        nullptr_t promotion_id; // "8e71b8bb-f142-4ab5-9925-c04357abbaa5",
        bool requires_autoscaling; // true,
        int storage_limit; // 0,
        nullptr_t subscription_end_at; // null,
        float total_archived_storage_limit; // 1000000000000,
        nullptr_t total_lifetime_file_limit; // null,
        nullptr_t total_member_limit; // null,
        int total_project_limit; // null,
        float total_storage_limit; // 500000000000,
        int total_user_limit; // 1,
        string updated_at; // "2021-11-03T21:40:16.563706Z",
        int user_limit; // 0


        /// <summary>
        /// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
        /// return true if all variables existe or if will return false 
        /// this function is not working for nested variables yet
        /// </summary>
        /// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
            #pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("account_id");
            StructureVariables.push_back("archived_storage_limit");
            StructureVariables.push_back("balance");
            StructureVariables.push_back("cancellation_option");
            StructureVariables.push_back("cancellation_reason");
            StructureVariables.push_back("cancelled_at");
            StructureVariables.push_back("deleted_at");
            StructureVariables.push_back("id;");
            StructureVariables.push_back("inserted_at");
            StructureVariables.push_back("last_payment_at");
            StructureVariables.push_back("member_limit");
            StructureVariables.push_back("next_bill_at");
            StructureVariables.push_back("on_trial");
            StructureVariables.push_back("plan");
            StructureVariables.push_back("plan_id");
            StructureVariables.push_back("promotion");
            StructureVariables.push_back("promotion_expires_at");
            StructureVariables.push_back("promotion_id");
            StructureVariables.push_back("requires_autoscaling");
            StructureVariables.push_back("storage_limit");
            StructureVariables.push_back("subscription_end_at");
            StructureVariables.push_back("total_archived_storage_limit");
            StructureVariables.push_back("total_lifetime_file_limit");
            StructureVariables.push_back("total_member_limit");
            StructureVariables.push_back("total_project_limit");
            StructureVariables.push_back("total_storage_limit");
            StructureVariables.push_back("total_user_limit");
            StructureVariables.push_back("updated_at");
            StructureVariables.push_back("user_limit");
            #pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_subscription;

    typedef struct __Account
    {
        vector<string> Parameters;
        string _type; // "account",
        string account_default_color; //  "#ff4040",
        _account_settings account_settings;
        int archived_storage; // 0,
        _available_features available_features;
        string billing_emails; // null,
        string city; // null,
        int collaborator_count; // 1,
        int collaborator_role_count; // 0,
        string company_address; //  null,
        string company_name; //  null,
        string country; //  null,
        string deleted_at; //  null,
        string delinquent_at; //  null,
        string display_name; //"Austin's Account",
        int duration; // 0,
        int file_count; // 0,
        int folder_count; //0,
        int frames; // 0,
        string id; // "95c5cf50-af7f-4c58-948e-0b5519b07880",
        string image; // null,
        string image_128; // null,
        string image_256; // null,
        string image_32; // null,
        string image_64; //null,
        string inserted_at; // "2021-11-03T21:38:25.734180Z",
        std::vector<std::string> invoice_emails; // [] ,
        int lifetime_file_count; // 0,
        string line1; // null,
        string line2; // null,
        string locked_at; // null,
        int member_count; //  1,
        string mfa_enforced_at; // null,
        _offline_config offline_config;
        _owner owner;
        string owner_id; // "c623173a-dcf1-45ac-a1ae-c6ee8e9bc966",
        _plan plan;
        string postal_code; //null,
        int project_count; // 2,
        nullptr_t resource_id; // null,
        string revenue_reported_at; // null,
        int reviewer_count; // 0,
        string state; // null,
        int storage; // 0,
        _subscription subscription;
        int team_count; // 1,
        string unpaid_at; // null,
        string updated_at; // "2021-11-03T21:38:25.734180Z",
        string upload_url; // "https://frameio-uploads-production.s3-accelerate.amazonaws.com/accounts/95c5cf50-af7f-4c58-948e-0b5519b07880/account_image_original.jpg?x-amz-meta-request_id=FrhJRSiIAn0DhicBKWWL&x-amz-meta-resource_id=95c5cf50-af7f-4c58-948e-0b5519b07880&x-amz-meta-resource_type=account&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAZ5BPIQ3GJVSAXJ5T%2F20211117%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20211117T085025Z&X-Amz-Expires=86400&X-Amz-SignedHeaders=content-type%3Bhost%3Bx-amz-acl&X-Amz-Signature=049617285624ec4e6690b9f4cbb5a527b15337d5ff1130684a5a19a85f838c56",
        int user_count; // 1,
        string vat; // null,
        int version; // 3

        /// <summary>
        /// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
        /// return true if all variables existe or if will return false 
        /// this function is not working for nested variables yet
        /// </summary>
        /// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
            #pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("account_default_color");
            StructureVariables.push_back("account_settings");
            StructureVariables.push_back("archived_storage");
            StructureVariables.push_back("available_features");
            StructureVariables.push_back("billing_emails");
            StructureVariables.push_back("city");
            StructureVariables.push_back("collaborator_count");
            StructureVariables.push_back("collaborator_role_count");
            StructureVariables.push_back("company_address");
            StructureVariables.push_back("company_name");
            StructureVariables.push_back("country");
            StructureVariables.push_back("deleted_at");
            StructureVariables.push_back("delinquent_at");
            StructureVariables.push_back("display_name");
            StructureVariables.push_back("duration");
            StructureVariables.push_back("file_count");
            StructureVariables.push_back("folder_count");
            StructureVariables.push_back("frames");
            StructureVariables.push_back("id");
            StructureVariables.push_back("image");
            StructureVariables.push_back("image_128");
            StructureVariables.push_back("image_256");
            StructureVariables.push_back("image_32");
            StructureVariables.push_back("image_64");
            StructureVariables.push_back("inserted_at");
            StructureVariables.push_back("invoice_emails");
            StructureVariables.push_back("lifetime_file_count");
            StructureVariables.push_back("line1"); 
            StructureVariables.push_back("line2"); 
            StructureVariables.push_back("locked_at");
            StructureVariables.push_back("member_count");
            StructureVariables.push_back("mfa_enforced_at");
            StructureVariables.push_back("offline_config");
            StructureVariables.push_back("owner");
            StructureVariables.push_back("owner_id");
            StructureVariables.push_back("plan");
            StructureVariables.push_back("postal_code");
            StructureVariables.push_back("project_count");
            StructureVariables.push_back("resource_id");
            StructureVariables.push_back("revenue_reported_at");
            StructureVariables.push_back("reviewer_count");
            StructureVariables.push_back("state");
            StructureVariables.push_back("storage");
            StructureVariables.push_back("subscription");
            StructureVariables.push_back("team_count");
            StructureVariables.push_back("unpaid_at");
            StructureVariables.push_back("updated_at"); 
            StructureVariables.push_back("upload_url"); 
            StructureVariables.push_back("user_count"); 
            StructureVariables.push_back("vat");
            StructureVariables.push_back("version");
            #pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }

    }_Account;

    typedef struct __data_points {
        vector<string> Parameters;
        string order;
        string _type;
        int value;
        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
            #pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("order");
            StructureVariables.push_back("value");
           
            #pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_data_points;

    typedef struct __watermark_blocks {
        vector<string> Parameters;
        string _type;
        double alpha;
        vector<_data_points> data_points;
        string font_size;
        string name;
        string position;
        string position_reference_point;
        int position_x;
        int position_y;
        string scroll_text;
        string text_alignment;
        string text_color;
        bool text_shadow;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
            #pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("alpha");
            StructureVariables.push_back("data_points");
            StructureVariables.push_back("font_size");
            StructureVariables.push_back("name");
            StructureVariables.push_back("position");
            StructureVariables.push_back("position_reference_point");
            StructureVariables.push_back("position_x");
            StructureVariables.push_back("position_y");
            StructureVariables.push_back("scroll_text");
            StructureVariables.push_back("text_alignment");
            StructureVariables.push_back("text_color");
            StructureVariables.push_back("text_shadow");

            #pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }

    }_watermark_blocks;

    typedef struct __session_watermark_template {
        vector<string> Parameters;
        string _type;
        string account_id;
        bool app_default;
        string creator_id;
        string id;
        string name;
        vector<__watermark_blocks> default_session_watermark_template;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("account_id");
            StructureVariables.push_back("app_default");
            StructureVariables.push_back("creator_id");
            StructureVariables.push_back("id");
            StructureVariables.push_back("name");
            StructureVariables.push_back("default_session_watermark_template");

#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_session_watermark_template;

    typedef struct __admin_only_actions {

        vector<string> Parameters;
        string _type;
        bool lifecycle;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("lifecycle");
           
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_admin_only_actions;

    typedef struct __text {
        vector<string> Parameters;
        string _type;
        double alpha;
        vector<string> lines;
        string position;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("alpha");
            StructureVariables.push_back("lines");
            StructureVariables.push_back("position");

#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_text;

    typedef struct __imageInfo {
        vector<string> Parameters;
        string _type;
        string bucket;
        string key;
        string type;
        bool uploaded;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("bucket");
            StructureVariables.push_back("key");
            StructureVariables.push_back("type");
            StructureVariables.push_back("uploaded");

#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }

    }_imageInfo;

    typedef struct __image {
        vector<string> Parameters;
        string _type;
        double alpha;
        _imageInfo imageInfo;
        string position;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("alpha");
            StructureVariables.push_back("imageInfo");
            StructureVariables.push_back("position");

#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }

    }_image;

    typedef struct __watermark {
        vector<string> Parameters;
        string _type;
        _image image;
        _text text;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("image");
            StructureVariables.push_back("text");

#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_watermark;

    typedef struct __email_branding {
        vector<string> Parameters;
        string _type;
        string accent_color;
        string background_color;
        _imageInfo image;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("accent_color");
            StructureVariables.push_back("background_color");
            StructureVariables.push_back("image");
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_email_branding;

    typedef struct __Teams {
        vector<string> Parameters;
        string _type;
        string account_id;
        bool disable_sbwm_internally;
        string creator_id;
        int storage;
        string default_font_color;
        string access;
        string font_color;
        //    "user_role" : { },
        int file_count;
        _session_watermark_template default_session_watermark_template;
        string link;
        int asset_lifecycle_policy;
        string upload_url;
        _admin_only_actions admin_only_actions;
        string deleted_at;
        string default_background_color;
        int storage_limit;
        string location;
        string name;
        string default_color;
        //    "slack_webhook" : { },
        string image_64;
        int archived_storage;
        string image_128;
        bool solo;
        _Account account;
        string image_32;
        string inserted_at;
        string updated_at;
        int tproject_count;
        int duration;
        string team_image;
        int collaborator_count;
        string bio;
        _watermark watermark;
        string color;
        _email_branding email_branding;
        int folder_count;
        bool dark_theme;
        string default_session_watermark_template_id;
        _session_watermark_template  session_watermark_templates;
        string id;
        int member_limit;
        int frames;
        int member_count;
        string image_256;
        string background_color;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
            #pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("account_id");
            StructureVariables.push_back("disable_sbwm_internally");
            StructureVariables.push_back("creator_id");
            StructureVariables.push_back("storage");
            StructureVariables.push_back("default_font_color");
            StructureVariables.push_back("access");
            StructureVariables.push_back("font_color");
            //    "user_role" : { },
            StructureVariables.push_back("file_count");
            StructureVariables.push_back("default_session_watermark_template");
            StructureVariables.push_back("link");
            StructureVariables.push_back("asset_lifecycle_policy");
            StructureVariables.push_back("upload_url");
            StructureVariables.push_back("admin_only_actions");
            StructureVariables.push_back("deleted_at");
            StructureVariables.push_back("default_background_color");
            StructureVariables.push_back("storage_limit");
            StructureVariables.push_back("location");
            StructureVariables.push_back("name");
            StructureVariables.push_back("default_color");
            //    "slack_webhook" : { },
            StructureVariables.push_back("image_64");
            StructureVariables.push_back("archived_storage");
            StructureVariables.push_back("image_128");
            StructureVariables.push_back("solo");
            StructureVariables.push_back("account");
            StructureVariables.push_back("image_32");
            StructureVariables.push_back("inserted_at");
            StructureVariables.push_back("updated_at");
            StructureVariables.push_back("tproject_count");
            StructureVariables.push_back("duration");
            StructureVariables.push_back("team_image");
            StructureVariables.push_back("collaborator_count");
            StructureVariables.push_back("bio");
            StructureVariables.push_back("watermark");
            StructureVariables.push_back("color");
            StructureVariables.push_back("email_branding");
            StructureVariables.push_back("folder_count");
            StructureVariables.push_back("dark_theme");
            StructureVariables.push_back("default_session_watermark_template_id");
            StructureVariables.push_back("session_watermark_templates");
            StructureVariables.push_back("id");
            StructureVariables.push_back("member_limit");
            StructureVariables.push_back("frames");
            StructureVariables.push_back("member_count");
            StructureVariables.push_back("image_256");
            StructureVariables.push_back("background_color");
            #pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_teams;

    // form this struct downword I am not adding all the json field but only the relvant ones ( in case we need any new variable we can added)

    typedef struct __project_preferences {
        vector<string> Parameters;
        string _type;
        bool collaborator_can_download;
        bool collaborator_can_invite;
        bool collaborator_can_share;
        bool deleted_at;
        bool devices_enabled;
        string id;
        string  inserted_at;
        bool notify_on_new_asset;
        bool notify_on_new_collaborator;
        bool notify_on_new_comment;
        bool notify_on_new_mention;
        bool notify_on_updated_label;
        bool notify_slack;
        string project_id;
        string updated_at;
        string user_id;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("collaborator_can_download");
            StructureVariables.push_back("collaborator_can_invite");
            StructureVariables.push_back("collaborator_can_share");
            StructureVariables.push_back("deleted_at");
            StructureVariables.push_back("devices_enabled");
            StructureVariables.push_back("id");
            StructureVariables.push_back("inserted_at");
            StructureVariables.push_back("notify_on_new_asset");
            StructureVariables.push_back("notify_on_new_collaborator");
            StructureVariables.push_back("notify_on_new_comment");
            StructureVariables.push_back("notify_on_new_mention");
            StructureVariables.push_back("notify_on_updated_label");
            StructureVariables.push_back("notify_slack");
            StructureVariables.push_back("project_id");
            StructureVariables.push_back("updated_at");
            StructureVariables.push_back("user_id");
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }

    }_project_preferences;

    typedef struct __Project {

        vector<string> Parameters;
        string _type;
        string id;
        string name;
        string owner_id;
        bool Private;
        _project_preferences project_preferences;
        string root_asset_id;
        string team_id;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("id");
            StructureVariables.push_back("name");
            StructureVariables.push_back("owner_id");
            StructureVariables.push_back("private");
            StructureVariables.push_back("project_preferences");
            StructureVariables.push_back("root_asset_id");
            StructureVariables.push_back("team_id");
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_Project;

    typedef struct __Roles {
        vector<string> Parameters;
        string _type;
        bool admin;
        string id;
        bool  sales;
        bool support;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("id");
            StructureVariables.push_back("admin");
            StructureVariables.push_back("sales");
            StructureVariables.push_back("support");
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_Roles;

    typedef struct __Creator {
        vector<string> Parameters;
        string _type;
        string digest_frequency;
        string image_32;
        string image_128;
        bool from_google;
        string mfa_enforced_at;
        string email;
        string name;
        string image_64;
        string timezone_value;
        string account_id;
        string updated_at;
        string image_256;
        string user_hash;
        string upload_url;

        string profile_image;
        string first_login_at;
        string joined_via;
        string id;
        string next_digest_date;
        string  last_seen;
        string  inserted_at;
        _Roles roles;
        string user_default_color;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("digest_frequency");
            StructureVariables.push_back("image_32");
            StructureVariables.push_back("image_128");
            StructureVariables.push_back("from_google");
            StructureVariables.push_back("mfa_enforced_at");
            StructureVariables.push_back("email");
            StructureVariables.push_back("name");
            StructureVariables.push_back("image_64");
            StructureVariables.push_back("timezone_value");
            StructureVariables.push_back("account_id");
            StructureVariables.push_back("updated_at");
            StructureVariables.push_back("image_256");
            StructureVariables.push_back("user_hash");
            StructureVariables.push_back("upload_url");
            StructureVariables.push_back("profile_image");
            StructureVariables.push_back("first_login_at");
            StructureVariables.push_back("joined_via");
            StructureVariables.push_back("id");
            StructureVariables.push_back("next_digest_date");
            StructureVariables.push_back("last_seen");
            StructureVariables.push_back("inserted_at");
            StructureVariables.push_back("roles");
            StructureVariables.push_back("user_default_color");
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_Creator;

    typedef struct __required_transcodes {
        vector<string> Parameters;
        string _type;
        bool cover;
        vector<string> finalized;
        bool h264_1080_best;
        bool h264_2160;
        bool h264_360;
        bool h264_540;
        bool h264_720;
        bool image_full;
        bool image_high;
        bool page_proxy;
        bool thumb;
        bool thumb_540;
        bool thumb_orig_ar_540;
        bool thumb_scrub;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("cover");
            StructureVariables.push_back("finalized");
            StructureVariables.push_back("h264_1080_best");
            StructureVariables.push_back("h264_2160");
            StructureVariables.push_back("h264_360");
            StructureVariables.push_back("h264_540");
            StructureVariables.push_back("h264_720");
            StructureVariables.push_back("image_full");
            StructureVariables.push_back("image_high");
            StructureVariables.push_back("page_proxy");
            StructureVariables.push_back("thumb");
            StructureVariables.push_back("thumb_540");
            StructureVariables.push_back("thumb_orig_ar_540");
            StructureVariables.push_back("thumb_scrub");
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_required_transcodes;

    typedef struct __user_permissions {
        vector<string> Parameters;
        string _type;
        bool can_download;
        bool can_modify_template;
        bool can_public_share_presentation;
        bool can_public_share_review_link;
        bool can_share_downloadable_presentation;
        bool can_share_downloadable_review_link;
        bool can_share_unwatermarked_presentation;
        bool can_share_unwatermarked_review_link;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("can_download");
            StructureVariables.push_back("can_modify_template");
            StructureVariables.push_back("can_public_share_presentation");
            StructureVariables.push_back("can_public_share_review_link");
            StructureVariables.push_back("can_share_downloadable_presentation");
            StructureVariables.push_back("can_share_downloadable_review_link");
            StructureVariables.push_back("can_share_unwatermarked_presentation");
            StructureVariables.push_back("can_share_unwatermarked_review_link");
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_user_permissions;

    typedef struct __Assest {
        vector<string> Parameters;
        string _type;
        string account_id;
        string asset_type;
        string archive_from;
        bool bundle;
        string bundle_view;
        string cover_asset_id;
        _Creator creator;
        string id;
        int index;
        bool is_bundle_child;
        bool is_hls_required;
        bool is_session_watermarked;
        int item_count;
        string label;
        string name;
        string original;
        string project_id;     
        _required_transcodes required_transcodes;
        string team_id;
        _user_permissions user_permissions;
        string type;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("account_id");
            StructureVariables.push_back("asset_type");
            StructureVariables.push_back("archive_from");
            StructureVariables.push_back("bundle");
            StructureVariables.push_back("bundle_view");
            StructureVariables.push_back("cover_asset_id");
            StructureVariables.push_back("creator");
            StructureVariables.push_back("id");
            StructureVariables.push_back("index");
            StructureVariables.push_back("is_bundle_child");
            StructureVariables.push_back("is_hls_required");
            StructureVariables.push_back("is_session_watermarked");
            StructureVariables.push_back("item_count");
            StructureVariables.push_back("label");
            StructureVariables.push_back("name");
            StructureVariables.push_back("original");
            StructureVariables.push_back("project_id");
            StructureVariables.push_back("required_transcodes");
            StructureVariables.push_back("team_id");
            StructureVariables.push_back("user_permissions");
            StructureVariables.push_back("type");
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_Assest;

    typedef struct __User {
        vector<string> Parameters;
        string _type;
        string account_id;
        string email;
        string id;
        string name;
        _Roles roles;

        /// <summary>
/// This function search the Variables vector to find if the user has inserted a variables that does not belong to this structure type
/// return true if all variables existe or if will return false 
/// this function is not working for nested variables yet
/// </summary>
/// <param name="Variables"></param>
        bool CheckVariablesExistence(vector<string> Variables)
        {
#pragma region adding variables vector
            vector<string> StructureVariables;
            //StructureElements.push_back("Parameters");
            StructureVariables.push_back("_type");
            StructureVariables.push_back("account_id");
            StructureVariables.push_back("email");
            StructureVariables.push_back("id");
            StructureVariables.push_back("name");
            StructureVariables.push_back("roles");
#pragma endregion adding variables vector
            return CheckVariablesExistenceInStructure(Variables, StructureVariables);
        }
    }_User;

    bool AddParsingParametersToJson(json& response_json, vector<string>  Parameters);
    void to_json(json& j, const _Authorization& p);
    void from_json(const json& j, _Authorization& p); 
    void to_json(json& j, const _account_settings& p);
    void from_json(const json& j, _account_settings& p); 
    void to_json(json& j, const _available_features& p);
    void from_json(const json& j, _available_features& p);
    void to_json(json& j, const _offline_config& p);
    void from_json(const json& j, _offline_config& p);
    void to_json(json& j, const _owner& p);
    void from_json(const json& j, _owner& p);
    void to_json(json& j, const _plan& p);
    void from_json(const json& j, _plan& p);
    void to_json(json& j, const _promotion& p);
    void from_json(const json& j, _promotion& p);
    void to_json(json& j, const _subscription& p);
    void from_json(const json& j, _subscription& p);
    void to_json(json& j, const _Account& p);
    void from_json(const json& j, _Account& p);

    void to_json(json& j, const _data_points& p);
    void from_json(const json& j, _data_points& p);
    void to_json(json& j, const  _watermark_blocks& p);
    void from_json(const json& j, _watermark_blocks& p);
    void to_json(json& j, const  _session_watermark_template& p);
    void from_json(const json& j, _session_watermark_template& p);
    void to_json(json& j, const  _admin_only_actions& p);
    void from_json(const json& j, _admin_only_actions& p);
    void to_json(json& j, const _text& p);
    void from_json(const json& j, _text& p);
    void to_json(json& j, const  _imageInfo& p);
    void from_json(const json& j, _imageInfo& p);
    void to_json(json& j, const _image& p);
    void from_json(const json& j, _image& p);
    void to_json(json& j, const  _watermark& p);
    void from_json(const json& j, _watermark& p);
    void to_json(json& j, const  _email_branding& p);
    void from_json(const json& j, _email_branding& p);
    void to_json(json& j, const  _teams& p);
    void from_json(const json& j, _teams& p);

    void to_json(json& j, const  _project_preferences& p);
    void from_json(const json& j, _project_preferences& p);
    void to_json(json& j, const  _Project& p);
    void from_json(const json& j, _Project& p);

    void to_json(json& j, const  _Roles& p);
    void from_json(const json& jj, _Roles& p);

    void to_json(json& j, const  _Creator& p);
    void from_json(const json& jj, _Creator& p);

    void to_json(json& j, const _required_transcodes& p);
    void from_json(const json& jj, _required_transcodes& p);

    void to_json(json& j, const _user_permissions& p);
    void from_json(const json& jj, _user_permissions& p);

    void to_json(json& j, const _Assest& p);
    void from_json(const json& jj, _Assest& p);

    void to_json(json& j, const _User& p);
    void from_json(const json& jj, _User& p);

}
#endif