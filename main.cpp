#include <iostream>
#include <stdlib.h>
#include <nlohmann/json.hpp>
#include <filesystem>
#include "FrameIONamespace.h"
#include "FrameIOClient.h"

using json = nlohmann::json;

int main(int argc, char **argv)
{
	// ===========    	TODO 	============= 
	// automate finding corresponding folder 
	// handle bad response from api callback
	// optimize returned value
	// linking proxy and iso files -- version stacking i.e uploading asset into asset
	// sync feature for proxy files 
	// uploading chunk of bytes instead of full file
	// pausing upload then continue later on
	// ======================================
	std::ifstream ifs("user_input.json");
    //cout << ifs.rdbuf()<< endl;
    
	json user_input_json = json::parse(ifs);
    //cout << user_input_json.dump(4)<< endl;
    //std::cout<<user_input_json["file_name"].dump() <<user_input_json["file_name"] <<std::endl;
	FrameIOClient client((std::string)user_input_json["token"]);

    std::string AuthorizationBuffer;
    std::string GetAccountBuffer;
    _User user;
    _Authorization my_Authorization;
    std::vector <_Account> myAccounts;
    _Account SpeciAccounts;
    std::vector <_teams> myteam;
    std::vector <_Project> myprojects;
    std::vector <_Assest> myFolders;
    std::vector <_Assest> myAssest;

    json _clinetdata;
    json _accounts;
    json _account;
    json _teams;
    json _teamProject;
    json _folder_structure;
    json _Assest;
    json _uploadFile;

    cout<< "fetch_User_data" << endl;
    client.fetch_User_data(user, _clinetdata);
    //cout<<_clinetdata.dump(4)<<std::endl;

    // get all accounts
    cout<< "fetch_accounts" << endl;
    client.fetch_accounts(myAccounts, _accounts);
    // get a specific account 

    cout<< "fetch_account" << endl;
    client.fetch_account(SpeciAccounts, myAccounts[0].id, _account);
    // get a teams in account 
    cout<< "fetch_teams" << endl;
    client.fetch_teams(myteam, SpeciAccounts.id, _teams);
    
    //get all projects for a certain team
    cout<< "fetch_team_project" << endl;
    client.fetch_team_project(myprojects, myteam[0].id, _teamProject);

    cout<< "fetch_folder_structure" << endl;
    client.fetch_folder_structure(myFolders, myprojects[1].root_asset_id, _folder_structure);
    
    cout<< "fetch_list_assets" << endl;
    client.fetch_list_assets(myAssest, myprojects[1].root_asset_id, _Assest);

    cout<< "Upload_Asset" << endl; 
    client.Upload_Asset(myprojects[1].root_asset_id, (std::string)user_input_json["file_name"], _uploadFile);

    cout<< "Upload_Asset successful" << endl; 
    return 0;
}