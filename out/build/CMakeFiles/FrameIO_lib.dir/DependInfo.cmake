# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/FrameIOClient.cpp" "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/out/build/CMakeFiles/FrameIO_lib.dir/FrameIOClient.cpp.o"
  "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/FrameIONamespace.cpp" "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/out/build/CMakeFiles/FrameIO_lib.dir/FrameIONamespace.cpp.o"
  "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/HttpWrapper.cpp" "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/out/build/CMakeFiles/FrameIO_lib.dir/HttpWrapper.cpp.o"
  "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/MimeTypes.cpp" "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/out/build/CMakeFiles/FrameIO_lib.dir/MimeTypes.cpp.o"
  "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/main.cpp" "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/out/build/CMakeFiles/FrameIO_lib.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BOOST_ALL_NO_LIB"
  "BOOST_FILESYSTEM_DYN_LINK"
  "JSON_DIAGNOSTICS=0"
  "JSON_USE_IMPLICIT_CONVERSIONS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../JsonLibrary/include/nlohmann"
  "../../curl/include/curl"
  "../../JsonLibrary/single_include"
  "../../curl/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/developer/Desktop/FramIO_Lib_Git/frameio_lib/out/build/curl/lib/CMakeFiles/libcurl.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
