#include "FrameIOClient.h"
#include "FrameIONamespace.h"
#include "MimeTypes.h"

using namespace FrameIONamespace;
using json = nlohmann::json;
namespace fs = boost::filesystem ;

FrameIOClient::FrameIOClient() : mywrapper("fio-u-SWOHewMWNplt0shAvWt5ZY3_4Kdwx0yitp5IjtDb779aMS8_X_091Jcac4Hd6GZT")
{
}

FrameIOClient::FrameIOClient(std::string my_token) : mywrapper(my_token)
{

}

bool FrameIOClient::fetch_User_data(_User& user, json& response_json)
{
    std::string call = "https://api.frame.io/v2/me";
    bool status = mywrapper.api_call(response_json, call, Request_Type::GET, "");
    if (response_json.empty() == false && status == true)
    {
        // need to check the parameters types 
        vector<string>  parameters;
        parameters.push_back("_type");
        parameters.push_back("account_id");
        parameters.push_back("id");
        parameters.push_back("email");
        parameters.push_back("name");


        if (!AddParsingParametersToJson(response_json, parameters))
        {
            return false;
        }

        user = response_json.get<_User>();

        vector<string>().swap(parameters); // to delete vector
        return status;
    }
    return false;
}

/// <summary>
/// this will fetch the User’s Accounts
/// </summary>
/// <param name="myAccounts"></param>
/// <param name="response_json"></param>
/// <returns></returns>
bool FrameIOClient::fetch_accounts(std::vector <_Account>& myAccounts, json& response_json)
{
    std::string call = "https://api.frame.io/v2/accounts";
    bool status = mywrapper.api_call(response_json, call, Request_Type::GET, "");

    // check if respones is not empty
    if (response_json.empty() == false && status == true)
    {
        // need to check the parameters types 
        vector<string>  parameters;
        parameters.push_back("_type");
        parameters.push_back("display_name");
        parameters.push_back("id");
        parameters.push_back("owner");
        parameters.push_back("owner.email");
        parameters.push_back("owner.name");


        if (!AddParsingParametersToJson(response_json, parameters))
        {
            return false;
        }

        myAccounts = response_json.get<vector<_Account>>();

        vector<string>().swap(parameters); // to delete vector
        return status;
    }
    return false;
}

/// <summary>
/// this will fetch a specific User Account
/// </summary>
/// <param name="myAccount"></param>
/// <param name="account_id"></param>
/// <param name="response_json"></param>
/// <returns></returns>
bool FrameIOClient::fetch_account(_Account& myAccount, std::string account_id, json& response_json)
{
    std::string call = "https://api.frame.io/v2/accounts/" + account_id;
    bool status = mywrapper.api_call(response_json, call, Request_Type::GET, "");
    
    // check if respones is not empty
    if (response_json.empty() == false && status == true)
    {
        vector<string>  parameters;
        parameters.push_back("_type");
        parameters.push_back("display_name");
        parameters.push_back("id");
        parameters.push_back("owner");
        parameters.push_back("owner.email");
        parameters.push_back("owner.name");
        if (!AddParsingParametersToJson(response_json, parameters))
        {
            return false;
        }

        myAccount = response_json.get<_Account>();

        vector<string>().swap(parameters); // to delete vector
        return status;
    }
    return false;
}

/// <summary>
/// this will fetch Teams within the Account
/// </summary>
/// <param name="myteams"></param>
/// <param name="account_id"></param>
/// <param name="response_json"></param>
/// <returns></returns>
bool FrameIOClient::fetch_teams(std::vector <_teams>& myteams, std::string account_id, json& response_json)
{
    std::string call = "https://api.frame.io/v2/accounts/" + account_id + "/teams";
    bool status = mywrapper.api_call(response_json, call, Request_Type::GET, "");
    
    // check if respones is not empty
    if (response_json.empty() == false && status == true)
    {
        vector<string>  parameters;
        parameters.push_back("_type");
        parameters.push_back("id");
        parameters.push_back("name");
        parameters.push_back("team_image");
        if (!AddParsingParametersToJson(response_json, parameters))
        {
            return false;
        }

        myteams = response_json.get<vector<_teams>>();

        vector<string>().swap(parameters); // to delete vector
        return status;
    }
    return false;
}

/// <summary>
/// This will fetch all the Projects within the Team
/// </summary>
/// <param name="myproject"></param>
/// <param name="team_id"></param>
/// <param name="response_json"></param>
/// <returns></returns>
bool FrameIOClient::fetch_team_project(std::vector <_Project>& myproject, std::string team_id, json& response_json)
{
    std::string call = "https://api.frame.io/v2/teams/" + team_id + "/projects";
    bool status = mywrapper.api_call(response_json, call, Request_Type::GET, "");
   
    // check if respones is not empty
    if (response_json.empty() == false && status == true)
    {
        vector<string>  parameters;
        parameters.push_back("_type");
        parameters.push_back("id");
        parameters.push_back("name");
        parameters.push_back("root_asset_id");
        parameters.push_back("private");

        if (!AddParsingParametersToJson(response_json, parameters))
        {
            return false;
        }

        myproject = response_json.get<vector<_Project>>();
        return status;
    }
    return false;
}

/// <summary>
/// This will list out all the folders in a project
/// </summary>
/// <param name="response_json"></param>
/// <param name="root_asset_id"></param>
/// <returns></returns>
bool FrameIOClient::fetch_folder_structure(std::vector <_Assest>& myFolders, std::string root_asset_id, json& response_json)
{
    //https://api.frame.io/v2/assets/{{folder_id}}/children?type=folder  // we can use this to scan assests using folder id
    std::string call = "https://api.frame.io/v2/assets/" + root_asset_id + "/children?type=folder";
    bool status = mywrapper.api_call(response_json, call, Request_Type::GET, "");
    // check if respones is not empty
    if (response_json.empty() == false && status == true)
    {
        vector<string>  parameters;
        parameters.push_back("_type");
        parameters.push_back("id");
        parameters.push_back("name");

        if (!AddParsingParametersToJson(response_json, parameters))
        {
            return false;
        }

        myFolders = response_json.get<vector<_Assest>>();
        return status;
    }
    return false;
}

/// <summary>
/// this will list all assests and folder in a project
/// </summary>
/// <param name="myAssest"></param>
/// <param name="root_asset_id"></param>
/// <param name="response_json"></param>
/// <returns></returns>
bool FrameIOClient::fetch_list_assets(std::vector <_Assest>& myAssest, std::string root_asset_id, json& response_json)
{
    std::string call = "https://api.frame.io/v2/assets/" + root_asset_id + "/children?";
    bool status = mywrapper.api_call(response_json, call, Request_Type::GET, "");
    // check if respones is not empty
    if (response_json.empty() == false && status == true)
    {
        vector<string>  parameters;
        parameters.push_back("_type");
        parameters.push_back("id");
        parameters.push_back("name");

        if (!AddParsingParametersToJson(response_json, parameters))
        {
            return false;
        }

        myAssest = response_json.get<vector<_Assest>>();
        return status;
    }
    return false;
}

json FrameIOClient::build_asset_info(std::string file_path)
{
    json asset_info;
    char *full_path = realpath(file_path.c_str(), NULL);
    cout<< "File Name" << endl;
    cout<< fs::path(full_path).filename().string() << endl;
    asset_info["file_path"] = full_path;
    asset_info["file_name"] = fs::path(full_path).filename().string();
    asset_info["file_size"] = fs::file_size(full_path);
    asset_info["mimetype"] = MimeTypes::getType(full_path);
    std::cout<<asset_info.dump(4)<<std::endl;
    // asset_info["mimetype"] = "video/mp4";
    return asset_info;
}

bool FrameIOClient::CreateAsset(json& asset_Info, string folder_id, json& response_json){

    json payload_json;
    payload_json["type"] = "file";
    payload_json["name"] = asset_Info["file_name"];
    payload_json["filesize"] = asset_Info["file_size"];
    payload_json["filetype"] = asset_Info["mimetype"];

    // this just create the asset at framIO but doesn't upload it
    std::string call = "https://api.frame.io/v2/assets/" + folder_id + "/children";
    bool status = mywrapper.api_call(response_json, call, Request_Type::POST, payload_json.dump());

    //add urls into json
    asset_Info["upload_urls"] = response_json["upload_urls"];

    return status;
}

bool FrameIOClient::Upload_Asset(std::string folder_id, std::string file_name, json& response_json)
{
    // std::string folder_id = "b5e6434c-4f97-4f2d-81d1-97038f6844d1";

    json Asset_response_json;
    json asset_info = build_asset_info(file_name);
    if (CreateAsset(asset_info, folder_id, Asset_response_json))
    {
        // upload file
        mywrapper.Upload_asset_handler(response_json, asset_info);
        return true;
    }
    return false;
}