#include "HttpWrapper.h"


HttpWrapper::HttpWrapper() : MY_TOKEN("fio-u-SWOHewMWNplt0shAvWt5ZY3_4Kdwx0yitp5IjtDb779aMS8_X_091Jcac4Hd6GZT")
{
}

HttpWrapper::HttpWrapper(std::string my_token) : MY_TOKEN(my_token)
{
}


std::size_t HttpWrapper::WriteCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

std::size_t HttpWrapper::Writefunc(const char* in, std::size_t size, std::size_t num, std::string* out)
{
    const std::size_t totalBytes(size * num);
    out->append(in, totalBytes);
    return totalBytes;
}

bool HttpWrapper::api_call(json& response_json, std::string endpoint = "", Request_Type method = Request_Type::GET, std::string payload = "")
{
    // Initialization
    std::string httpData{};
    CURL* curl;
    struct curl_slist* slist = NULL;
    curl = curl_easy_init();
    std::string message_to_display = "";

    // setting URL for communication
    curl_easy_setopt(curl, CURLOPT_URL, (endpoint).c_str());
    switch (method)
    {
    case Request_Type::GET:
        //message_to_display += "GET request: ";
        break;
    case Request_Type::POST:
        //if post request then add payload to body
        // message_to_display += "POST request: ";
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, payload.c_str());
        slist = curl_slist_append(slist, "content-type: application/json");
        break;
        //default:
            //message_to_display += "Weird request";
    }
    // add headers
    slist = curl_slist_append(slist, ("Authorization: Bearer " + MY_TOKEN).c_str());
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &httpData);
    //curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp); // write callback to file
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);
    // perform request and receive response
    const CURLcode result = curl_easy_perform(curl);

    // handling request
    //json response_json;
    if (result == CURLE_OK)
    {
        message_to_display += "successful \n";
        response_json = json::parse(httpData);
        //cout << response_json << endl;
        curl_easy_cleanup(curl);
        return true;
    }
    else
    {
        message_to_display += "Error: " + result + (std::string)curl_easy_strerror(result) + "\n";
        response_json = "{}";
        curl_easy_cleanup(curl);
        return false;
    }

    //printf("%s", message_to_display.c_str());

    //curl_easy_cleanup(curl);
    //return true;
}

std::size_t HttpWrapper::Read_callback(char* ptr, size_t size, size_t nmemb, void* stream)
{
    size_t retcode;
    curl_off_t nread;

    /* in real-world cases, this would probably get this data differently
     as this fread() stuff is exactly what the library already would do
     by default internally */
    retcode = fread(ptr, size, nmemb, (FILE*)stream);

    nread = (curl_off_t)retcode;

    fprintf(stderr, "*** We read %" CURL_FORMAT_CURL_OFF_T " bytes from file\n", nread);

    return retcode;
}

std::vector<int> HttpWrapper::Calculate_chunks(int total_size, int chunk_count)
{
    // calculates the size of the each chunk
    int chunck_size = ceil(total_size / chunk_count);

    std::vector<int> chunk_offset;
    for (int i = 0; i < chunk_count; ++i)
    {
        chunk_offset.push_back(i * chunck_size);
    }

    return chunk_offset;
}

void HttpWrapper::Upload_api_call(std::string host, std::string file_path, std::string asset_filetype, int chunck_size)
{
    // opening file
    FILE* fd;
    fd = fopen(file_path.c_str(), "rb"); /* open file to upload */
    if (!fd)
    {
        std::cout << "error opening file" << std::endl;
        return; /* cannot continue */
    }

    // Initialization
    std::string httpData{};
    CURL* curl;
    struct curl_slist* slist = NULL;
    curl = curl_easy_init();
    std::string message_to_display = "Uploading Chunck ";

    // setting URL for communication
    curl_easy_setopt(curl, CURLOPT_URL, host.c_str());
    // curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
    curl_easy_setopt(curl, CURLOPT_READDATA, fd);
    curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)chunck_size);
    // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); /* enable verbose for easier tracing */
    slist = curl_slist_append(slist, ("content-type: " + asset_filetype).c_str());
    slist = curl_slist_append(slist, "x-amz-acl: private");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, Writefunc);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &httpData);
    curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L);

    // perform request and receive response
    const CURLcode result = curl_easy_perform(curl);

    // handling request
    json response_json;
    if (result == CURLE_OK)
    {
        message_to_display += "successful \n";
    }
    else
    {
        std::cout << result << std::endl;
        message_to_display += "Error: " + result + (std::string)curl_easy_strerror(result) + "\n";
    }
    //printf("%s", message_to_display.c_str());
    curl_easy_cleanup(curl);
}

void HttpWrapper::Upload_asset_handler(json& response_json, json& asset_creation_response)
{
    int total_size = (int)asset_creation_response["file_size"];
    std::vector<std::string> upload_urls = asset_creation_response["upload_urls"];
    std::vector<int> chunk_offsets = Calculate_chunks(total_size, upload_urls.size());

    for (int i = 0; i < chunk_offsets.size(); i++)
    {
        // std::cout << "URL: " << upload_urls[i] << '\n'
        //           << "file_path: " << file_path << '\n'
        //           << asset_creation_response["filetype"] << std::endl;

        Upload_api_call(upload_urls[i], asset_creation_response["file_path"], asset_creation_response["mimetype"], total_size);
    }
}