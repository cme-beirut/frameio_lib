#include <nlohmann/json.hpp>
#include <curl/curl.h>
#include <filesystem>
#include <boost/filesystem.hpp>
#include <stdlib.h>
#include <iostream>
#include <vector> 
#include "HttpWrapper.h"
#include "FrameIONamespace.h"

using namespace FrameIONamespace;
using json = nlohmann::json;
namespace fs = boost::filesystem;

class FrameIOClient
{

public:
    FrameIOClient();
    FrameIOClient(std::string my_token);
    bool fetch_User_data(_User& user, json& response_json);                // get details of client account
    bool fetch_accounts(std::vector<_Account> &myAccounts, json& response_json);                    // get all accounts 
    bool fetch_account(_Account& myAccount, std::string account_id, json& response_json);  // get a specific account
    bool fetch_teams(std::vector <_teams>& myteams, std::string account_id, json& response_json);  // get teams info
    bool fetch_team_project(std::vector <_Project>& myteams, std::string team_id, json& response_json);            // get team projects
    bool fetch_folder_structure(std::vector <_Assest>& myFolders, std::string root_asset_id, json& response_json);   // get folder structure 
    bool fetch_list_assets(std::vector <_Assest>& myAssest, std::string root_asset_id, json& response_json);        // get a list of assets within a folder
    bool CreateAsset(json& asset_Info, string folder_id, json& response_json);
    bool Upload_Asset(std::string folder_id, std::string file_name, json& response_json);// upload assets to desired folder 

private:
    static const std::string host;
    const std::string MY_TOKEN; 
    HttpWrapper mywrapper;

    json build_asset_info(std::string file_path); // utility function to get info about a file, used for request
};